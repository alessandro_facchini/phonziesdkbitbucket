//  Created by Karen Lusinyan on 26/07/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

@class CP_ParkingInfo;

typedef NS_ENUM(NSInteger, PHNZSDKEventType) {
    PHNZSDKEventTypeUserDidStartParking=0,
    PHNZSDKEventTypeUserDidStopParking,
    PHNZSDKEventTypeUserDidExtendParking
};

@interface PHNZSDKEvent : NSObject

@property (nonatomic) PHNZSDKEventType type;
@property (nonatomic, strong) id object;

@end

@interface PHNZSDKEventManager : NSObject

+ (void)notifyWithEvent:(PHNZSDKEvent *)event;

@end
