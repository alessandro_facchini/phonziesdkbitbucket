//  Created by Karen Lusinyan on 06/05/16.
//  Copyright © 2016 Lenis. All rights reserved.

#import "CP_User.h"
#import <CoreLocation/CoreLocation.h>

@interface CP_User (Ext)

///-----------------
/// @name Properties
///-----------------

@property (nonatomic) BOOL needsRefreshProfilePhoto;

///-----------------
/// @name Properties
///-----------------

@property (nonatomic) CLLocation *parkingLocation;

@end
