// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Loc_DictionaryResponse.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@interface Loc_DictionaryResponseID : NSManagedObjectID {}
@end

@interface _Loc_DictionaryResponse : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) Loc_DictionaryResponseID *objectID;

@property (nonatomic, strong, nullable) NSNumber* language;

@property (atomic) int64_t languageValue;
- (int64_t)languageValue;
- (void)setLanguageValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSDate* last_update;

@property (nonatomic, strong, nullable) NSNumber* version;

@property (atomic) int64_t versionValue;
- (int64_t)versionValue;
- (void)setVersionValue:(int64_t)value_;

@end

@interface _Loc_DictionaryResponse (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveLanguage;
- (void)setPrimitiveLanguage:(NSNumber*)value;

- (int64_t)primitiveLanguageValue;
- (void)setPrimitiveLanguageValue:(int64_t)value_;

- (NSDate*)primitiveLast_update;
- (void)setPrimitiveLast_update:(NSDate*)value;

- (NSNumber*)primitiveVersion;
- (void)setPrimitiveVersion:(NSNumber*)value;

- (int64_t)primitiveVersionValue;
- (void)setPrimitiveVersionValue:(int64_t)value_;

@end

@interface Loc_DictionaryResponseAttributes: NSObject 
+ (NSString *)language;
+ (NSString *)last_update;
+ (NSString *)version;
@end

NS_ASSUME_NONNULL_END
