//  Created by Karen Lusinyan on 28/06/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

#import "PHNZCell.h"

@interface PHNZCell0 : PHNZCell

@property (readonly, weak, nonatomic) IBOutlet UILabel *title;
@property (readonly, weak, nonatomic) IBOutlet UILabel *subtitle;

@end
