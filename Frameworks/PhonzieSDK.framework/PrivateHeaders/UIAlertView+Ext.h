//  Created by Karen Lusinyan on 12/05/16.
//  Copyright © 2016 Lenis. All rights reserved.

@interface UIAlertView (Ext)

- (void)showWithCompletion:(void(^)(UIAlertView *alertView, NSInteger buttonIndex))completion;

- (void)showWithCompletionWhenDismissed:(void(^)(UIAlertView *alertView, NSInteger buttonIndex))completion;

@end
