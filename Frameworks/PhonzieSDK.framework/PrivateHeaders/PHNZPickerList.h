//  Created by Karen Lusinyan on 23/03/2017.
//  Copyright © 2017 Lenis. All rights reserved.

typedef void(^PHNZPickerCloseCompletion)(void);
typedef void(^PHNZPickerSelectCompletion)(NSIndexPath *indexPath);
typedef void(^PHNZPickerScrollUpCompletion)(void);
typedef void(^PHNZPickerScrollDownCompletion)(void);

@interface PHNZPickerList : UIView

@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) NSDictionary *propertiesToFetch;                  // if nil, then "self.items" elements considered
@property (nonatomic) BOOL needsDynamicCellHeight;                              // default NO, cell height is 60
@property (nonatomic, getter=isSearchEnabled) BOOL setSearchEnabled;
@property (nonatomic, getter=isMultipleSearchEnabled) BOOL setMultipleSearchEnabled;

@property (nonatomic, copy) PHNZPickerCloseCompletion closeCompletion;
@property (nonatomic, copy) PHNZPickerSelectCompletion selectCompletion;
@property (nonatomic, copy) PHNZPickerScrollUpCompletion scrollUpCompletion;
@property (nonatomic, copy) PHNZPickerScrollDownCompletion scrollDownCompletion;

- (void)setMaximumCharacter:(int)maxCharSearchBox1
          maxCharSearchBox2:(int)maxCharSearchBox2;

@end
