//  Created by Karen Lusinyan on 27/09/16.
//  Copyright © 2016 Lenis. All rights reserved.

#import <CoreLocation/CoreLocation.h>

@class CP_ParkingInfo;

@interface CP_ParkingLocation : NSObject <NSCoding>

// prepaid parking
@property (nonatomic, strong) CP_ParkingInfo *parkingInfo;

// custom parking
@property (nonatomic, strong) CLLocation *location;
@property (nonatomic, strong) NSDate *startDate;

// common
@property (nonatomic, strong) NSString *address;

// check if parking is active
@property (nonatomic) BOOL isValid;

- (BOOL)isPrepaidParking;

@end
