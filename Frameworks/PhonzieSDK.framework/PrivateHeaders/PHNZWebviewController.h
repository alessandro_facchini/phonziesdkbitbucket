//  Created by Simone Ferrini on 23/01/15.
//  Modified by Karen Lusinyan.
//  Copyright (c) 2015 Lenis. All rights reserved.

typedef void(^Callback)(void);

@interface PHNZWebviewController : UIViewController

@property (nonatomic, strong) NSURL *url;

@property (nonatomic, copy) Callback callback;

+ (instancetype)instance;

@end
