//  Created by Karen Lusinyan on 27/06/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

#import "PHNZCell.h"

@interface PHNZCell8 : PHNZCell

@property (readonly, weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@end
