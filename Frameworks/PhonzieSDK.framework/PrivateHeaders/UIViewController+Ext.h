//  Created by Karen Lusinyan on 26/04/16.
//  Copyright © 2016 Lenis. All rights reserved.

typedef void(^DismissBlock)(NSError *error);

@interface UIViewController (Ext)

- (void)displayContentController:(UIViewController *)content
                       container:(UIView *)container
                      completion:(void(^)(void))completion;

- (void)hideContentController:(UIViewController *)content;

- (DismissBlock)presentController:(UIViewController *)controller
              withBackgroundColor:(UIColor *)color
                         andAlpha:(CGFloat)alpha
                presentCompletion:(void(^)(void))presentCompletion;

@end
