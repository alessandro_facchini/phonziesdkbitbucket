//  Created by Karen Lusinyan on 27/09/16.
//  Copyright © 2016 Lenis. All rights reserved.

#import "CP_ParkingInfo.h"
#import <CoreLocation/CoreLocation.h>

@interface CP_ParkingInfo (Ext)

- (CLLocation *)parkingLocation;

- (BOOL)locationExists;

- (BOOL)isActive;

@end
