//  Created by Karen Lusinyan on 21/06/16.
//  Copyright © 2016 Lenis. All rights reserved.

// server call request headers
#import "PhonzieServiceProxy.h"
#import "PHNZAPIEndpoint.h"

#import <CoreLocation/CoreLocation.h>

extern NSString * const kPHNZServerCallErrorTypeUnknown;
extern NSString * const kPHNZServerCallErrorTypeRemoteServerError;
extern NSString * const kPHNZServerCallErrorTypeNetworkConnection;

typedef void(^PHNZServerCallSuccessHandler)(id response, NSString *method);
typedef void(^PHNZServerCallFailureHandler)(id exception, NSString *method);

/*
typedef NS_ENUM(NSInteger, PHNZThirdPartRegistrationProvieder) {
    PHNZThirdPartRegistrationProviederFacebook=0
};
//*/

@class PHNZServerRequest;

@protocol PHNZServerCallProtocol <NSObject>

@required
- (void)successBlock:(void(^)(id data, NSString *method))block;

- (void)failureBlock:(void(^)(NSException *exception, NSString *method))block;

@end

@interface PHNZServerCall : NSObject

+ (instancetype)instance;

+ (instancetype)sharedInstance;

+ (void)cancelRequest:(PHNZServerRequest *)service;

+ (void)cancelRequests:(NSArray *)requests;

+ (void)setDebugMode:(BOOL)debugMode;

+ (TransactionStatus)transactionStatus:(NSException *)exception;

+ (PHNZServerRequest *)addOperation:(SEL)operation
                        withRequest:(id)request
                          showAlert:(BOOL)showAlert
                             sender:(id)sender
                            success:(PHNZServerCallSuccessHandler)success
                            failure:(PHNZServerCallFailureHandler)failure
                         completion:(void(^)(void))completion;

///-------------------
/// @name PHONZIE APP
///-------------------

// get app configuration
+ (PHNZServerRequest *)GetAppConfiguration:(NSString *)clientVersion
                           clientVersionId:(int)clientVersionId
                            clientSystemId:(int)clientSystemId
                                  latitude:(NSString *)latitude
                                 longitude:(NSString *)longitude
                           clientProgramId:(int)clientProgramId
                                 showAlert:(BOOL)showAlert
                                    sender:(id)sender
                                   success:(PHNZServerCallSuccessHandler)success
                                   failure:(PHNZServerCallFailureHandler)failure;
// get cashback configuration
+ (PHNZServerRequest *)GetCashbackConfiguration:(BOOL)showAlert
                                         sender:(id)sender
                                        success:(PHNZServerCallSuccessHandler)success
                                        failure:(PHNZServerCallFailureHandler)failure;

// update push token
+ (PHNZServerRequest *)UpdatePushToken:(NSString *)pushToken
                            pushUserId:(NSString *)pushUserId
                          pushDeviceId:(NSString *)pushDeviceId
                             showAlert:(BOOL)showAlert
                                sender:(id)sender
                               success:(PHNZServerCallSuccessHandler)success
                               failure:(PHNZServerCallFailureHandler)failure;

// insert user
+ (PHNZServerRequest *)InsertUser:(CP_SaveUser *)user
                         username:(NSString *)username
                 referrerUsername:(NSString *)referrerUsername
               referrerCarLicense:(NSString *)referrerCarLicense
                        showAlert:(BOOL)showAlert
                           sender:(id)sender
                          success:(PHNZServerCallSuccessHandler)success
                          failure:(PHNZServerCallFailureHandler)failure;

// get user
+ (PHNZServerRequest *)GetUser:(NSString *)username
                      password:(NSString *)password
                     showAlert:(BOOL)showAlert
                        sender:(id)sender
                       success:(PHNZServerCallSuccessHandler)success
                       failure:(PHNZServerCallFailureHandler)failure;

// get list of cities
+ (PHNZServerRequest *)GetListOfCities:(NSString *)latitude
                             longitude:(NSString *)longitude
                             showAlert:(BOOL)showAlert
                                sender:(id)sender
                               success:(PHNZServerCallSuccessHandler)success
                               failure:(PHNZServerCallFailureHandler)failure;

// get closest cities
+ (PHNZServerRequest *)GetClosestCities:(NSString *)latitude
                              longitude:(NSString *)longitude
                              showAlert:(BOOL)showAlert
                                 sender:(id)sender
                                success:(PHNZServerCallSuccessHandler)success
                                failure:(PHNZServerCallFailureHandler)failure __deprecated_msg("use GetListOfCities:longitude:success:failure");

// get city services
+ (PHNZServerRequest *)GetCityServices:(NSString *)city
                              latitude:(NSString *)latitude
                             longitude:(NSString *)longitude
                      lastModifiedDate:(NSString *)lastModifiedDate
                             showAlert:(BOOL)showAlert
                                sender:(id)sender
                               success:(PHNZServerCallSuccessHandler)success
                               failure:(PHNZServerCallFailureHandler)failure;

// send nagging email to municipality
+ (PHNZServerRequest *)SendNaggingEmailToMunicipality:(NSString *)cityName
                                          serviceName:(NSString *)serviceName
                                            showAlert:(BOOL)showAlert
                                               sender:(id)sender
                                              success:(PHNZServerCallSuccessHandler)success
                                              failure:(PHNZServerCallFailureHandler)failure;

// password recovery
+ (PHNZServerRequest *)PasswordRecovery:(NSString *)username
                              showAlert:(BOOL)showAlert
                                 sender:(id)sender
                       success:(PHNZServerCallSuccessHandler)success
                       failure:(PHNZServerCallFailureHandler)failure;

// password recovery
+ (PHNZServerRequest *)UpdatePassword:(NSString *)password
                       updatePassword:(NSString *)updatePassword
                            showAlert:(BOOL)showAlert
                               sender:(id)sender
                                success:(PHNZServerCallSuccessHandler)success
                                failure:(PHNZServerCallFailureHandler)failure;

// is registered user
+ (PHNZServerRequest *)IsRegisteredUser:(NSString *)username
                               password:(NSString *)password
                              showAlert:(BOOL)showAlert
                                 sender:(id)sender
                                success:(PHNZServerCallSuccessHandler)success
                                failure:(PHNZServerCallFailureHandler)failure;

// recharge creedit
+ (PHNZServerRequest *)RechargeCredit:(double)amount
                       additionalCost:(double)additionalCost
                      paymentMethodId:(int)paymentMethodId
         paymentMethodDescrServerCode:(NSString *)paymentMethodDescrServerCode
                            showAlert:(BOOL)showAlert
                               sender:(id)sender
                              success:(PHNZServerCallSuccessHandler)success
                              failure:(PHNZServerCallFailureHandler)failure;

// enroll
+ (PHNZServerRequest *)EnrollMultimethod:(double)amount
                          additionalCost:(double)additionalCost
                         paymentMethodId:(int)paymentMethodId
            paymentMethodDescrServerCode:(NSString *)paymentMethodDescrServerCode
                               showAlert:(BOOL)showAlert
                                  sender:(id)sender
                                 success:(PHNZServerCallSuccessHandler)success
                                 failure:(PHNZServerCallFailureHandler)failure;

// update user payment method
+ (PHNZServerRequest *)UpdateUserPaymentMethod:(NSString *)paymentPreference
                                     showAlert:(BOOL)showAlert
                                        sender:(id)sender
                                       success:(PHNZServerCallSuccessHandler)success
                                       failure:(PHNZServerCallFailureHandler)failure;

// recharge automatically
+ (PHNZServerRequest *)UpdateUserAutoTopUp:(int)amount
                                 showAlert:(BOOL)showAlert
                                    sender:(id)sender
                                   success:(PHNZServerCallSuccessHandler)success
                                   failure:(PHNZServerCallFailureHandler)failure;

// revoke user enrollment
+ (PHNZServerRequest *)RevokeUserEnrollment:(BOOL)showAlert
                                     sender:(id)sender
                                    success:(PHNZServerCallSuccessHandler)success
                                    failure:(PHNZServerCallFailureHandler)failure;

/*
// third part registration
+ (PHNZServerRequest *)ThirdPartRegistration:(NSString *)username
                                  payerEmail:(NSString *)payerEmail
                                     payerID:(NSString *)payerID
                            referrerUsername:(NSString *)referrerUsername
                          referrerCarLicense:(NSString *)referrerCarLicense
                        registrationProvider:(PHNZThirdPartRegistrationProvieder)registrationProvider
                                providerData:(NSDictionary *)providerData
                                     success:(PHNZServerCallSuccessHandler)success
                                     failure:(PHNZServerCallFailureHandler)failure;
//*/

// get dictionary
+ (PHNZServerRequest *)GetDictionary:(int)language
                           showAlert:(BOOL)showAlert
                              sender:(id)sender
                             success:(PHNZServerCallSuccessHandler)success
                             failure:(PHNZServerCallFailureHandler)failure;

// buy credit with points
+ (PHNZServerRequest *)BuyCreditWithPoints:(int)pointsToSpend
                                 showAlert:(BOOL)showAlert
                                    sender:(id)sender
                                   success:(PHNZServerCallSuccessHandler)success
                                   failure:(PHNZServerCallFailureHandler)failure;

// email service report
+ (PHNZServerRequest *)EmailServiceReport:(BOOL)showAlert
                                   sender:(id)sender
                                  success:(PHNZServerCallSuccessHandler)success
                                  failure:(PHNZServerCallFailureHandler)failure;

// get parkign areas
+ (PHNZServerRequest *)GetParkingAreas:(NSString *)cityId
                              latitude:(NSString *)latitude
                             longitude:(NSString *)longitude
                             showAlert:(BOOL)showAlert
                                sender:(id)sender
                               success:(PHNZServerCallSuccessHandler)success
                               failure:(PHNZServerCallFailureHandler)failure;

// get lastParkings
+ (PHNZServerRequest *)GetLastParkings:(BOOL)showAlert
                                sender:(id)sender
                               success:(PHNZServerCallSuccessHandler)success
                               failure:(PHNZServerCallFailureHandler)failure;

// stop prepaid parking
+ (PHNZServerRequest *)StopPrepaidParking:(NSString *)parkingId
                                showAlert:(BOOL)showAlert
                                   sender:(id)sender
                                  success:(PHNZServerCallSuccessHandler)success
                                  failure:(PHNZServerCallFailureHandler)failure;

// get prepaid parking
+ (PHNZServerRequest *)GetPrepaidParkingAmount:(NSString *)carLicense
                                        cityId:(NSString *)cityId
                                        areaID:(NSString *)areaID
                                    spotNumber:(NSString *)spotNumber
                               durationMinutes:(int)durationMinutes
                                      latitude:(NSString *)latitude
                                     longitude:(NSString *)longitude
                                     showAlert:(BOOL)showAlert
                                        sender:(id)sender
                                       success:(PHNZServerCallSuccessHandler)success
                                       failure:(PHNZServerCallFailureHandler)failure;

// start prepaid parking
+ (PHNZServerRequest *)StartPrepaidParking:(NSString *)carLicense
                                    cityId:(NSString *)cityId
                                    areaID:(NSString *)areaID
                                spotNumber:(NSString *)spotNumber
                           durationMinutes:(int)durationMinutes
                                  latitude:(NSString *)latitude
                                 longitude:(NSString *)longitude
                                 showAlert:(BOOL)showAlert
                                    sender:(id)sender
                                   success:(PHNZServerCallSuccessHandler)success
                                   failure:(PHNZServerCallFailureHandler)failure;

// extend prepaid parking
+ (PHNZServerRequest *)ExtendPrepaidParking:(NSString *)parkingId
                          additionalMinutes:(int)additionalMinutes
                                  showAlert:(BOOL)showAlert
                                     sender:(id)sender
                                    success:(PHNZServerCallSuccessHandler)success
                                    failure:(PHNZServerCallFailureHandler)failure;

// get active tickets
+ (PHNZServerRequest *)GetActiveTickets:(NSString *)deviceId
                              showAlert:(BOOL)showAlert
                                 sender:(id)sender
                                success:(PHNZServerCallSuccessHandler)success
                                failure:(PHNZServerCallFailureHandler)failure;

// start timed ticket
+ (PHNZServerRequest *)StartTimedTicket:(NSString *)ticketClassId
                          passengerName:(NSString *)passengerName
                             lineNumber:(NSString *)lineNumber
                               terminus:(NSString *)terminus
                           discountCode:(NSString *)discountCode
                               deviceId:(NSString *)deviceId
                              showAlert:(BOOL)showAlert
                                 sender:(id)sender
                                success:(PHNZServerCallSuccessHandler)success
                                failure:(PHNZServerCallFailureHandler)failure;

// activate ticket
+ (PHNZServerRequest *)ActivateTicket:(NSString *)ticketId
                        passengerName:(NSString *)passengerName
                           lineNumber:(NSString *)lineNumber
                             terminus:(NSString *)terminus
                             deviceId:(NSString *)deviceId
                            showAlert:(BOOL)showAlert
                               sender:(id)sender
                              success:(PHNZServerCallSuccessHandler)success
                              failure:(PHNZServerCallFailureHandler)failure;

// get active box office tickets
+ (PHNZServerRequest *)GetActiveBoxOfficeTickets:(NSString *)deviceId
                                       showAlert:(BOOL)showAlert
                                          sender:(id)sender
                                         success:(PHNZServerCallSuccessHandler)success
                                         failure:(PHNZServerCallFailureHandler)failure;

// activate box office ticket
+ (PHNZServerRequest *)ActivateBoxOfficeTicket:(NSString *)ticketId
                                 passengerName:(NSString *)passengerName
                                    lineNumber:(NSString *)lineNumber
                                      terminus:(NSString *)terminus
                                      deviceId:(NSString *)deviceId
                                     showAlert:(BOOL)showAlert
                                        sender:(id)sender
                                       success:(PHNZServerCallSuccessHandler)success
                                       failure:(PHNZServerCallFailureHandler)failure;

// start box office ticket
+ (PHNZServerRequest *)StartBoxOfficeTicket:(NSString *)ticketClassId
                              passengerName:(NSString *)passengerName
                                 lineNumber:(NSString *)lineNumber
                                   terminus:(NSString *)terminus
                               discountCode:(NSString *)discountCode
                                   deviceId:(NSString *)deviceId
                                  showAlert:(BOOL)showAlert
                                     sender:(id)sender
                                    success:(PHNZServerCallSuccessHandler)success
                                    failure:(PHNZServerCallFailureHandler)failure;

// purchase gadgests
+ (PHNZServerRequest *)PurchaseAssorted:(int)numberOfStickers
                          numberOfDiscs:(int)numberOfDiscs
                       numberOfPointers:(int)numberOfPointers
                            totalAmount:(double)totalAmount
                              showAlert:(BOOL)showAlert
                                 sender:(id)sender
                                success:(PHNZServerCallSuccessHandler)success
                                failure:(PHNZServerCallFailureHandler)failure;

// purchase stickers
+ (PHNZServerRequest *)PurchaseStickers:(int)stickerAmount
                              showAlert:(BOOL)showAlert
                                 sender:(id)sender
                                success:(PHNZServerCallSuccessHandler)success
                                failure:(PHNZServerCallFailureHandler)failure;

// purchase discs
+ (PHNZServerRequest *)PurchaseParkingDiscs:(int)parkingDiscsNumber
                                  showAlert:(BOOL)showAlert
                                     sender:(id)sender
                                    success:(PHNZServerCallSuccessHandler)success
                                    failure:(PHNZServerCallFailureHandler)failure;

// shop purchase request
+ (PHNZServerRequest *)GetShopPurchase:(NSString *)keyCode
                             showAlert:(BOOL)showAlert
                                sender:(id)sender
                               success:(PHNZServerCallSuccessHandler)success
                               failure:(PHNZServerCallFailureHandler)failure;

// shop purcheses by user
+ (PHNZServerRequest *)GetShopPurchasesByUser:(int)startIndex
                                     endIndex:(int)endIndex
                                    showAlert:(BOOL)showAlert
                                       sender:(id)sender
                                      success:(PHNZServerCallSuccessHandler)success
                                      failure:(PHNZServerCallFailureHandler)failure;

// shop votes by user
+ (PHNZServerRequest *)GetShopVotesByUser:(int)startIndex
                                 endIndex:(int)endIndex
                                showAlert:(BOOL)showAlert
                                   sender:(id)sender
                                  success:(PHNZServerCallSuccessHandler)success
                                  failure:(PHNZServerCallFailureHandler)failure;
// get shop
+ (PHNZServerRequest *)GetShop:(int)shopId
                     showAlert:(BOOL)showAlert
                        sender:(id)sender
                       success:(PHNZServerCallSuccessHandler)success
                       failure:(PHNZServerCallFailureHandler)failure;
// get shop discount
+ (PHNZServerRequest *)GetShopDiscount:(int)discountId
                             showAlert:(BOOL)showAlert
                                sender:(id)sender
                               success:(PHNZServerCallSuccessHandler)success
                               failure:(PHNZServerCallFailureHandler)failure;

// get town discount
+ (PHNZServerRequest *)GetTownDiscount:(int)discountId
                             showAlert:(BOOL)showAlert
                                sender:(id)sender
                               success:(PHNZServerCallSuccessHandler)success
                               failure:(PHNZServerCallFailureHandler)failure;

/*
// get town services
+ (PHNZServerRequest *)GetTownServices:(NSString *)cityId
                    getFullShopDetails:(BOOL)getFullShopDetails
                      lastModifiedDate:(NSString *)lastModifiedDate
                       lastRequestDate:(NSString *)lastRequestDate
                            pageNumber:(int)pageNumber
                              pageSize:(int)pageSize
                             showAlert:(BOOL)showAlert
                                sender:(id)sender
                               success:(PHNZServerCallSuccessHandler)success
                               failure:(PHNZServerCallFailureHandler)failure;

// get town services update count
+ (PHNZServerRequest *)GetTownServicesCount:(NSString *)cityId
                           lastModifiedDate:(NSString *)lastModifiedDate
                                  showAlert:(BOOL)showAlert
                                    sender:(id)sender
                                    success:(PHNZServerCallSuccessHandler)success
                                    failure:(PHNZServerCallFailureHandler)failure;
 //*/

// udpate shop favour
+ (PHNZServerRequest *)UpdateShopFavour:(int)shopId
                              favourite:(BOOL)favourite
                              showAlert:(BOOL)showAlert
                                 sender:(id)sender
                                success:(PHNZServerCallSuccessHandler)success
                                failure:(PHNZServerCallFailureHandler)failure;

// update discount favour
+ (PHNZServerRequest *)UpdateDiscountFavour:(int)discountId
                                  favourite:(BOOL)favourite
                                  showAlert:(BOOL)showAlert
                                     sender:(id)sender
                                    success:(PHNZServerCallSuccessHandler)success
                                    failure:(PHNZServerCallFailureHandler)failure;
// insert shop vote
+ (PHNZServerRequest *)InsertShopVote:(CP_ShopVote *)vote
                            showAlert:(BOOL)showAlert
                               sender:(id)sender
                              success:(PHNZServerCallSuccessHandler)success
                              failure:(PHNZServerCallFailureHandler)failure;

// update card nfc
+ (PHNZServerRequest *)UpdateCardNFC:(NSString *)cardNFCToSet
                           showAlert:(BOOL)showAlert
                              sender:(id)sender
                             success:(PHNZServerCallSuccessHandler)success
                             failure:(PHNZServerCallFailureHandler)failure;

///-------------------
/// @name MERCHANT APP
///-------------------

// get shops by owner
+ (PHNZServerRequest *)GetShopsByOwner:(BOOL)showAlert
                                sender:(id)sender
                               success:(PHNZServerCallSuccessHandler)success
                               failure:(PHNZServerCallFailureHandler)failure;

// get user by nfc
+ (PHNZServerRequest *)GetUserByNFC:(NSString *)cardNumberNFC
                          showAlert:(BOOL)showAlert
                             sender:(id)sender
                            success:(PHNZServerCallSuccessHandler)success
                            failure:(PHNZServerCallFailureHandler)failure;

// shop parkign refund
+ (PHNZServerRequest *)GetProposedShopRefunds:(int)shopId
                                buyerUsername:(NSString *)buyerUsername
                                    showAlert:(BOOL)showAlert
                                       sender:(id)sender
                                      success:(PHNZServerCallSuccessHandler)success
                                      failure:(PHNZServerCallFailureHandler)failure;

// insert phonzie card
+ (PHNZServerRequest *)InsertPhonzieCardPurchase:(double)purchaseAmount
                                    refundAmount:(double)refundAmount
                                   buyerUsername:(NSString *)buyerUsername
                                        deviceID:(NSString *)deviceID
                                         keyCode:(NSString *)keyCode
                                          shopID:(int)shopID
                                       showAlert:(BOOL)showAlert
                                          sender:(id)sender
                                         success:(PHNZServerCallSuccessHandler)success
                                         failure:(PHNZServerCallFailureHandler)failure;

// update customer card
+ (PHNZServerRequest *)UpdateCustomersCardNFC:(NSString *)cardNFCToSet
                             customerUsername:(NSString *)customerUsername
                                    showAlert:(BOOL)showAlert
                                       sender:(id)sender
                                      success:(PHNZServerCallSuccessHandler)success
                                      failure:(PHNZServerCallFailureHandler)failure;

// renounce card nfc
+ (PHNZServerRequest *)RenounceCardNFC:(NSString *)cardNFCToSet
                             showAlert:(BOOL)showAlert
                                sender:(id)sender
                               success:(PHNZServerCallSuccessHandler)success
                               failure:(PHNZServerCallFailureHandler)failure;

// get prize list
+ (PHNZServerRequest *)GetPrizeList:(int)shopId
                          showAlert:(BOOL)showAlert
                             sender:(id)sender
                            success:(PHNZServerCallSuccessHandler)success
                            failure:(PHNZServerCallFailureHandler)failure;

///-----------------
/// @name PhonzieSDK
///-----------------

// get sdk token
+ (PHNZServerRequest *)SdkLogin:(NSString *)developerKey
                       username:(NSString *)username
                         isTest:(BOOL)isTest
                       language:(int)language
                     libraryKey:(NSString *)libraryKey
                       bundleId:(NSString *)bundleId
                       deviceId:(NSString *)deviceId
                      checkCode:(NSString *)checkCode
                       latitude:(NSString *)latitude
                      longitude:(NSString *)longitude
              deviceDescriprion:(NSString *)deviceDescriprion
                      showAlert:(BOOL)showAlert
                         sender:(id)sender
                        success:(PHNZServerCallSuccessHandler)success
                        failure:(PHNZServerCallFailureHandler)failure;

// get sdk batch data
+ (PHNZServerRequest *)GetSdkDataBatch:(NSString *)username
                                 token:(NSString *)token
                              latitude:(NSString *)latitude
                             longitude:(NSString *)longitude
                                cityId:(int)cityId
                     dictionaryVersion:(int)dictionaryVersion
                              language:(int)language
                        getFullDetails:(BOOL)getFullDetails
                         clientVersion:(NSString *)clientVersion
                       clientVersionId:(int)clientVersionId
                        clientSystemId:(int)clientSystemId
                       clientProgramId:(int)clientProgramId
                             showAlert:(BOOL)showAlert
                                sender:(id)sender
                               success:(PHNZServerCallSuccessHandler)success
                               failure:(PHNZServerCallFailureHandler)failure;

// is covered
+ (PHNZServerRequest *)isCovered:(NSString *)latitude
                       longitude:(NSString *)longitude
                       showAlert:(BOOL)showAlert
                          sender:(id)sender
                         success:(PHNZServerCallSuccessHandler)success
                         failure:(PHNZServerCallFailureHandler)failure;


// save crash report
+ (PHNZServerRequest *)SaveCrashReport:(NSString *)filename
                           fileContent:(NSString *)fileContent
                             showAlert:(BOOL)showAlert
                                sender:(id)sender
                               success:(PHNZServerCallSuccessHandler)success
                               failure:(PHNZServerCallFailureHandler)failure;

// recharge credit
+ (PHNZServerRequest *)RechargeCreditAPI:(double)amount
                       additionalCost:(double)additionalCost
                      paymentMethodId:(int)paymentMethodId
         paymentMethodDescrServerCode:(NSString *)paymentMethodDescrServerCode
                            showAlert:(BOOL)showAlert
                               sender:(id)sender
                              success:(PHNZServerCallSuccessHandler)success
                              failure:(PHNZServerCallFailureHandler)failure;

///------------------
/// @name Tracking
///------------------

// insert traffic tracking
+ (PHNZServerRequest *)InsertTrafficTracking:(TrafficTrackingRequest *)request
                                     success:(PHNZServerCallSuccessHandler)success
                                     failure:(PHNZServerCallFailureHandler)failure;

// batch calls of InsertTrafficTracking:success:success: performed in background
+ (void)performBetchTrackingRequests:(void (^)(UIBackgroundFetchResult result))completionHandler;


@end
