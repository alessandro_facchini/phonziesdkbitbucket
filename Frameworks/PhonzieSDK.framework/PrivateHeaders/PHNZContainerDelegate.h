//  Created by Karen Lusinyan on 21/06/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

#import "PHNZContainerDelegate.h"

@class CP_ParkingInfo;

typedef NS_ENUM(NSInteger, PHNZSDKViewMode) {
    PHNZSDKViewModeUnknown,
    PHNZSDKViewModeParking,
    PHNZSDKViewModeParkingDetails,
    PHNZSDKViewModeWallet,
    PHNZSDKViewModePurchases,
    PHNZSDKViewModeOffline
};

@protocol PHNZContainerDelegate <NSObject>

@property (nonatomic, assign) id<PHNZContainerDelegate> delegate;

@optional
- (void)updateParkingInfo;

- (void)setController:(id<PHNZContainerDelegate>)controller
            withTitle:(NSString *)title
          forViewMode:(PHNZSDKViewMode)viewMode;

- (void)removeController:(id<PHNZContainerDelegate>)controller
      showMainController:(BOOL)showMainController;

- (void)setSubtitleLabel:(NSString *)subtitleLabel
           subtitleValue:(NSString *)subtitleValue;

@end
