//  Created by Karen Lusinyan on 26/06/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

@class CP_User;
@class CP_City;
@class CP_ParkingArea;
@class CP_CityServices;
@class CP_TrackingEvent;

#import "PhonzieService_Enums.h"

@interface PHNZBatchData : NSObject

@property (nonatomic, strong) CP_User *user;
@property (nonatomic, strong) NSMutableArray *paymentMethods;
@property int dictionaryVersion;
@property (nonatomic, strong) NSMutableArray *requiredTrackingEvents;
@property (nonatomic, strong) NSMutableArray *activeParkings;
@property (nonatomic, copy) NSString *selectionDescription;
@property (nonatomic, strong) NSMutableArray *cities;
@property (nonatomic, strong) CP_CityServices *services;
@property (nonatomic, strong) CP_City *city;
@property (nonatomic, strong) NSMutableArray *areas;
@property BOOL isInsideCityBoundaries;
@property (nonatomic, strong) CP_ParkingArea *closestArea;
@property (nonatomic, strong) NSMutableArray *languageDictionary;

+ (instancetype)sharedInstance;

+ (void)reset;

/**
 *  This method return the generated tracking key with category and action
 */
+ (BOOL)trackingEnabledForCategory:(CP_TrafficTrackingCategory)category
                         andAction:(CP_TrafficTrackingAction)action;


@end
