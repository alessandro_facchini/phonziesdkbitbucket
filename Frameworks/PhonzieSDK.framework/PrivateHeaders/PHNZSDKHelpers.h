//  Created by Karen Lusinyan on 20/06/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

#import "PHNZServerCall.h"
#import "PHNZConstants.h"
#import "PHNZSDKParams.h"

@class CP_ParkingInfo;

@interface PHNZSDKHelpers : NSObject

+ (CP_ParkingInfo *)activeParking;

+ (void)updateParkingInfo:(CP_ParkingInfo *)parkingInfo createIfNeeded:(BOOL)createIfNeeded;

+ (void)forceReloadLastParkings;

+ (NSString *)username;

+ (NSString *)password;

+ (void)setIsAPI:(BOOL)isAPI;

+ (BOOL)isAPI;

// used in all SDK internal calls
+ (PHNZServerRequest *)refreshToken:(NSString *)username
                         libraryKey:(NSString *)libraryKey
                          showAlert:(BOOL)showAlert
                             sender:(id)sender
                         completion:(void (^)(BOOL success, NSString *username, NSString *token, NSException *exception))completion;

// used in isCovered and initial SDK server call
+ (PHNZServerRequest *)refreshToken:(NSString *)username
                           deviceId:(NSString *)deviceId
                          checkCode:(NSString *)checkCode
                         libraryKey:(NSString *)libraryKey
                          showAlert:(BOOL)showAlert
                             sender:(id)sender
                         completion:(void (^)(BOOL success, NSString *username, NSString *token, NSException *exception))completion;


+ (void)reset;

@end
