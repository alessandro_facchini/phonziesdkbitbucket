//  Created by Karen Lusinyan on 12/05/16.
//  Copyright © 2016 Lenis. All rights reserved.

#ifndef PHNZConstants_h
#define PHNZConstants_h

///------------------------
/// @name SDK configuration
///------------------------

#define SDK_CONFIGURATION_RELEASE 1
#if !SDK_CONFIGURATION_RELEASE
#warning SDK_CONFIGURATION_DEBUG
#endif

// TODO:: DISABLE FOR RELEASE
#define SDK_ENVIRONMENT_TEST !SDK_CONFIGURATION_RELEASE && 1
#if SDK_ENVIRONMENT_TEST
#warning sdk is in test environment
#endif

// TODO:: DISABLE FOR RELEASE
#define kDebugLogEnabled !SDK_CONFIGURATION_RELEASE
#if kDebugLogEnabled
#warning debug logging is 0N
#endif

// TODO:: DISABLE FOR RELEASE
#define kServerCallLogEnabled !SDK_CONFIGURATION_RELEASE
#if kServerCallLogEnabled
#warning server logging is 0N
#endif

// TODO:: ENABLE FOR RELEASE
#define kCrashRreportEnabled SDK_CONFIGURATION_RELEASE
#if !kCrashRreportEnabled
#warning crash report is OFF
#endif

///---------------------
/// @name SDK contstants
///---------------------

#if SDK_ENVIRONMENT_TEST
static NSString * const SDK_LIBRARY_KEY = @"2233";
static NSString * const API_LIBRARY_KEY = @"2233";

// uncomment to point db prod
//static NSString * const LIBRARY_KEY = @"AtTCBTYMvnFEx8dnRMqZ";
#else
static NSString * const SDK_LIBRARY_KEY = @"AtTCBTYMvnFEx8dnRMqZ";
static NSString * const API_LIBRARY_KEY = @"WDjN7PYTgNW0R837WL1V";

#endif

///------------------------
/// @name SDK notifications
///------------------------

static NSString * const kPHNZNotificaionUserDidStartParking  = @"kPHNZNotificaionUserDidStartParking";
static NSString * const kPHNZNotificaionUserDidStopParking   = @"kPHNZNotificaionUserDidStopParking";
static NSString * const kPHNZNotificaionUserDidExtendParking = @"kPHNZNotificaionUserDidExtendParking";

///-------------------------
/// @name Network contstants
///-------------------------

static NSString * const kPHNZWwebviewMobileActionKey = @"mobileAction";
static NSString * const kPHNZWwebviewMobileActionValueBack = @"back";

///------------------------------
/// @name Notification contstants
///------------------------------

// in-app notifications contstants

static NSString * const kPHNZNotificationLocalizzationUpdated = @"kPHNZNotificationLocalizzationUpdated";
static NSString * const kPHNZNotificationUserDidLogin = @"kPHNZNotificationUserDidLogin";
static NSString * const kPHNZNotificationUserDidLogout = @"kPHNZNotificationUserDidLogout";
static NSString * const kPHNZNotificationGetUser = @"kPHNZNotificationGetUser";
static NSString * const kPHNZNotificationMapViewRegionWillChange = @"kPHNZNotificationMapViewRegionWillChange";
static NSString * const kPHNZNotificationMapViewRegionDidChange = @"kPHNZNotificationMapViewRegionDidChange";
static NSString * const kPHNZNotificationNetworkStatusOffline = @"kPHNZNotificationNetworkStatusOffline";
static NSString * const kPHNZNotificationNetworkStatusOnline = @"kPHNZNotificationNetworkStatusOnline";

// remote and local notification contstants and types

typedef NS_ENUM(NSInteger, PHNZLocalNotificationType) {
    PHNZLocalNotificationTypeNotifyWhenExpiring=0
};

typedef NS_ENUM(NSInteger, PHNZLocalNotificationAction) {
    PHNZLocalNotificationActionShowActiveParking=0,
};

// user info
static NSString * const kPHNZLocalNotificationParkingIdKey = @"parkingId";
static NSString * const kPHNZLocalNotificationParkingUsernameKey = @"parkingUsername";
static NSString * const kPHNZLocalNotificationParkingCarLicenseKey = @"parkingCarLicense";
static NSString * const kPHNZLocalNotificationParkingStartDateKey = @"pakringStartDate";
static NSString * const kPHNZLocalNotificationParkingEndDateKey = @"pakringEndDate";

static NSString * const kPHNZLocalNotificationIdentifierKey = @"kPHNZLocalNotificationIdentifierKey";
static NSString * const kPHNZLocalNotificationTypeKey = @"kPHNZLocalNotificationTypeKey";
static NSString * const kPHNZLocalNotificationActionKey = @"kPHNZLocalNotificationActionKey";

///----------------------------
/// @name Exceptions contstants
///----------------------------

static NSString * const PHNZExceptionCodeDeveloperNotRegistered = @"SD001";
static NSString * const PHNZExceptionCodeAuthorizzationRequired = @"SD003";
static NSString * const PHNZExceptionCodeAuthorizzationCodeInvalid = @"SD004";
static NSString * const PHNZExceptionCodeSpotNumberIncorrect = @"PC018";
static NSString * const PHNZExceptionCodeCreditoInsufficiente = @"OP002";
static NSString * const PHNZExceptionCodeCreditCardNotEnrolled = @"OP003";
static NSString * const PHNZExceptionCodeCreditPayPalNotEnrolled = @"OP009";

#endif /* PHNZConstants_h */
