//  Modified by Karen Lusinyan.
//  Copyright (c) 2015 Lenis. All rights reserved.

@class CP_ParkingInfo;

@interface PHNZLocalNotification : NSObject

+ (void)addLocalNotification:(CP_ParkingInfo *)parkingInfo;

+ (void)removeLocalNotification:(CP_ParkingInfo *)parkingInfo;

+ (void)updateLocalNotification:(CP_ParkingInfo *)parkingInfo;

@end
