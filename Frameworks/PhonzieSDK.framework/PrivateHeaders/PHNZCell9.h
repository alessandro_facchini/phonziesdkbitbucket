//  Created by Karen Lusinyan on 10/07/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

#import "PHNZCell.h"

@interface PHNZCell9 : PHNZCell

@property (readonly, weak, nonatomic) IBOutlet UIPickerView *picker;
@property (nonatomic, strong) NSArray *pickerItems;

- (void)reloadPicker;

@end
