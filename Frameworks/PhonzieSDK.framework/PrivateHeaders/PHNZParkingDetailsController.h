//  Created by Karen Lusinyan on 29/06/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

#import "PHNZContentController.h"

@class CP_ParkingInfo;

typedef void(^DismissCallback)(BOOL needsReload);

@interface PHNZParkingDetailsController : PHNZContentController

@property (nonatomic, strong) CP_ParkingInfo *parkingInfo;

@property (nonatomic, copy) DismissCallback dimissCallback;

+ (instancetype)instance;

@end
