//  Created by Simone Ferrini on 21/04/15.
//  Copyright (c) 2015 Lenis. All rights reserved.

@interface PHNZErrorCodes : NSObject

+ (NSString *)messageFromErrorCode:(NSString *)code;

@end
