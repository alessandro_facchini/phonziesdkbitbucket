//  Created by Karen Lusinyan on 27/06/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

#import "PHNZCell.h"

@interface PHNZCell5 : PHNZCell

@property (readonly, weak, nonatomic) IBOutlet UIImageView *leftAccessoryView;
@property (readonly, weak, nonatomic) IBOutlet UILabel *label1;
@property (readonly, weak, nonatomic) IBOutlet UILabel *value1;
@property (readonly, weak, nonatomic) IBOutlet UILabel *value2;

@end
