//------------------------------------------------------------------------------
// <wsdl2code-generated>
// This code was generated by http://www.wsdl2code.com iPhone version 2.1
// Date Of Creation: 6/9/2017 6:36:10 AM
//
//  Please dont change this code, regeneration will override your changes
///<wsdl2code-generated>
//
//------------------------------------------------------------------------------
//
//This source code was auto-generated by Wsdl2Code Version
//

#import <Foundation/Foundation.h>


@interface CP_CreditCard : NSObject
{
}
@property (nonatomic, copy) NSString *cardNumber;
@property (nonatomic, copy) NSString *vendor;
@property (nonatomic, copy) NSString *expiryDate;
@property BOOL enrollmentSuccessful;
@property (nonatomic, copy) NSString *enrollmentErrorCode;
@property (nonatomic, copy) NSString *enrollmentErrorMessage;
@property (nonatomic, copy) NSString *cardCountry;
@property (nonatomic, copy) NSString *last4Digits;
@property (nonatomic, copy) NSString *auth;
@property (nonatomic, copy) NSString *payInst;
@property (nonatomic, copy) NSString *bankRef;
@property (nonatomic, copy) NSString *enrollmentTranId;
@property (nonatomic, copy) NSString *trackId;

-(NSString*)toString:(BOOL)addNameWrap;
-(id)initWithArray:(NSArray*)array;
-(void)encodeWithCoder:(NSCoder *)encoder;
-(id)copyWithZone:(NSZone *)zone;
-(id)initWithCoder:(NSCoder *)decoder;
@end
