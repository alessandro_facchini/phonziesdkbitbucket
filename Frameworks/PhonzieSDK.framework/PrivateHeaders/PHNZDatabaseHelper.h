//  Created by Karen Lusinyan on 22/06/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

typedef void(^CompletionBlock)(BOOL success, NSError *error);

@interface PHNZDatabaseHelper : NSObject

+ (void)reset;

+ (int)dictionaryVersion:(int)language;

+ (void)saveDictionary:(NSArray *)dictionary
           forLanguage:(int)language
               version:(int)version
            completion:(CompletionBlock)completion;

+ (NSString *)localizedString:(NSString *)key
                     language:(int)language;

@end
