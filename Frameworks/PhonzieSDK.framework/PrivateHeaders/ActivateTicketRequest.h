//------------------------------------------------------------------------------
// <wsdl2code-generated>
// This code was generated by http://www.wsdl2code.com iPhone version 2.1
// Date Of Creation: 6/9/2017 6:36:10 AM
//
//  Please dont change this code, regeneration will override your changes
///<wsdl2code-generated>
//
//------------------------------------------------------------------------------
//
//This source code was auto-generated by Wsdl2Code Version
//

#import <Foundation/Foundation.h>


@interface ActivateTicketRequest : NSObject
{
}
@property (nonatomic, copy) NSString *ticketId;
@property BOOL ignoreWarnings;
@property (nonatomic, copy) NSString *passengerName;
@property (nonatomic, copy) NSString *lineNumber;
@property (nonatomic, copy) NSString *terminus;
@property (nonatomic, copy) NSString *deviceId;
@property int language;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *password;

-(NSString*)toString:(BOOL)addNameWrap;
-(id)initWithArray:(NSArray*)array;
-(void)encodeWithCoder:(NSCoder *)encoder;
-(id)copyWithZone:(NSZone *)zone;
-(id)initWithCoder:(NSCoder *)decoder;
@end
