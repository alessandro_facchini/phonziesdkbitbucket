//  Created by Simone Ferrini on 10/02/15.
//  Copyright (c) 2015 Lenis. All rights reserved.

#import <Foundation/Foundation.h>

#define kSDKVersion @"1.0.0"

@interface PHNZUtility : NSObject

// identifica la versione di client, es: iOS 7, 1.0.0 (2)
+ (NSString *)clientVersion;

// identifica la piattaforma iOS (== 4)
+ (int)clientSystemId;

// identifica la versione di client iOS
+ (int)clientVersionId;

// identifica l'app Phonzie=0, Merchant=1
+ (int)clientProgramId;

+ (NSString *)appVersionForClient;

+ (NSString *)osVersion;

+ (NSString *)appName;

+ (NSString *)deviceId;

+ (NSString *)deviceName;

+ (NSString *)deviceDescription;

+ (NSString *)deviceLanguage;

+ (NSString *)bundleIdentifier;

+ (NSString *)locationWhenInUseDescription;

+ (int)languageCode;

+ (BOOL)isPhonzie;

+ (BOOL)isMerchant;

+ (BOOL)isPhonzieApp;

+ (BOOL)isPhonzieMerchantApp;

+ (BOOL)isPhonzieBetaApp;

+ (BOOL)isPhonzieMerchantBetaApp;

+ (BOOL)isSDK;

+ (NSString *)phonzieCardAppKey;

// push utils
+ (void)setPushUserId:(NSString *)pushUserId;

+ (void)setPushToken:(NSString *)pushToken;

+ (NSString *)getPushUserId;

+ (NSString *)getPushToken;

@end
