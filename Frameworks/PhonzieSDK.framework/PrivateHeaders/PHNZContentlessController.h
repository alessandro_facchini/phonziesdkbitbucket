//  Created by Karen Lusinyan on 03/07/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

@interface PHNZContentlessController : UIViewController

@property (nonatomic, copy) NSString *contentTitle;

+ (instancetype)instance;

@end
