//  Created by Karen Lusinyan on 20/06/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

#import "PHNZContentController.h"

@interface PHNZPurchasesController : PHNZContentController

+ (instancetype)instance;

@end
