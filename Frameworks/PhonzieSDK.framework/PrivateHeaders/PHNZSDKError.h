//  Created by Karen Lusinyan on 04/08/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

typedef NS_ENUM(NSInteger, PhonzieSDKErrorCode) {
    PhonzieSDKErrorCodeConnectionFailed     = 9000,
    PhonzieSDKErrorCodeNoInternetConnection = 9001,
    PhonzieSDKErrorCodeTooManyCalls         = 9002,
    PhonzieSDKErrorCodeConnectionTimeout    = 9003,
    PhonzieSDKErrorCodeException            = 9004,
    PhonzieSDKErrorCodeParametersMissing    = 9005,
    PhonzieSDKErrorCodeServerError          = 9006
};

@interface PHNZSDKError : NSObject

/////////////////////
// internal errors //
/////////////////////

// returns internal errors: ex. PhonzieSDKErrorCodeNoInternetConnection, PhonzieSDKErrorCodeConnectionTimeout
+ (NSError *)errorWithCode:(NSInteger)code;

///////////////////
// serevr errors //
///////////////////

// returns server errore with type PhonzieSDKErrorCodeException
+ (NSError *)errorWithException:(NSException *)exception;

+ (NSError *)errorWithReponse:(id)respons;

@end
