//  Created by Karen Lusinyan on 27/06/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

#import "PHNZCell.h"

@interface PHNZCell6 : PHNZCell

@property (readonly, weak, nonatomic) IBOutlet UILabel *label1;
@property (readonly, weak, nonatomic) IBOutlet UILabel *label2;
@property (readonly, weak, nonatomic) IBOutlet UILabel *label3;
@property (readonly, weak, nonatomic) IBOutlet UITextField *textField;
@property (readonly, weak, nonatomic) IBOutlet UIButton *button1;
@property (readonly, weak, nonatomic) IBOutlet UIButton *button2;
@property (readonly, weak, nonatomic) IBOutlet UIButton *button3;

@end
