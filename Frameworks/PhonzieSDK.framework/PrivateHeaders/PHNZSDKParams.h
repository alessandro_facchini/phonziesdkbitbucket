//  Created by Karen Lusinyan on 26/06/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

@interface PHNZSDKParams : NSObject

@property (nonatomic, copy) NSString *developerKey;

@property (nonatomic, copy) NSString *registeredUsername;

@property (nonatomic, copy) NSString *username;

@property (nonatomic, copy) NSString *token;

@property (nonatomic) BOOL isTest;

@property (nonatomic, copy) NSString *carLicense;

@property (nonatomic, strong) NSDate *parkingEndDate;

@property (nonatomic) NSInteger minutesForParkingDuration;

@property (nonatomic) NSInteger minutesForParkingExtension;

@property (nonatomic) NSInteger minutesForParkingExpiryNotification;

@property (nonatomic) BOOL disableAutoZoneDetection;

@property (nonatomic) BOOL disablePushNotificatons;

@property (nonatomic) double latitude;

@property (nonatomic) double longitude;

@property (nonatomic, copy) NSString *cityId;

+ (instancetype)sharedInstance;

+ (void)reset;

@end
