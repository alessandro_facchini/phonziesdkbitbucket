//  Created by Karen Lusinyan on 17/06/16.
//  Copyright © 2016 Lenis. All rights reserved.

#import "CP_Image.h"

@interface CP_Image (Ext)

@property (nonatomic) BOOL isNew;

- (NSString *)imageUrl;

- (NSString *)thumbUrl;

+ (NSArray *)imageUrls:(NSArray *)images;

+ (NSArray *)thumbUrls:(NSArray *)images;

@end
