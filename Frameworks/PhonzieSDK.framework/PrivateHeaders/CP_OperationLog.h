//------------------------------------------------------------------------------
// <wsdl2code-generated>
// This code was generated by http://www.wsdl2code.com iPhone version 2.1
// Date Of Creation: 6/9/2017 6:36:10 AM
//
//  Please dont change this code, regeneration will override your changes
///<wsdl2code-generated>
//
//------------------------------------------------------------------------------
//
//This source code was auto-generated by Wsdl2Code Version
//

#import <Foundation/Foundation.h>


@interface CP_OperationLog : NSObject
{
}
@property (nonatomic, copy) NSString *logId;
@property (nonatomic, copy) NSString *logType;
@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *userName;
@property double amount;
@property BOOL expired;
@property (nonatomic, copy) NSString *endDate;
@property (nonatomic, copy) NSString *userType;
@property (nonatomic, copy) NSString *pIVA;
@property (nonatomic, copy) NSString *cF;
@property (nonatomic, copy) NSString *companyName;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *cap;
@property int cityId;

-(NSString*)toString:(BOOL)addNameWrap;
-(id)initWithArray:(NSArray*)array;
-(void)encodeWithCoder:(NSCoder *)encoder;
-(id)copyWithZone:(NSZone *)zone;
-(id)initWithCoder:(NSCoder *)decoder;
@end
