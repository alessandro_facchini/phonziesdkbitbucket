//------------------------------------------------------------------------------
// <wsdl2code-generated>
// This code was generated by http://www.wsdl2code.com iPhone version 2.1
// Date Of Creation: 6/9/2017 6:36:10 AM
//
//  Please dont change this code, regeneration will override your changes
///<wsdl2code-generated>
//
//------------------------------------------------------------------------------
//
//This source code was auto-generated by Wsdl2Code Version
//

#import <Foundation/Foundation.h>
#import "PhonzieService_Enums.h"
#import "CP_ShopVote.h"
#import "CP_Image.h"
#import "CP_ShopDiscount.h"
#import "CP_ShopDiscount.h"
#import "CP_ShopProduct.h"
#import "CP_Image.h"
#import "CP_ShopVoteGroup.h"
#import "CP_Texts.h"
#import "CP_Texts.h"
#import "CP_Image.h"


@interface CP_Shop : NSObject
{
}
@property int shopId;
@property (nonatomic, copy) NSString *shopName;
@property (nonatomic, strong) NSMutableArray *commoditiesSectorIds;
@property (nonatomic, copy) NSString *shopDescription;
@property (nonatomic, copy) NSString *mainPhotoURL;
@property (nonatomic, copy) NSString *logoURL;
@property (nonatomic, copy) NSString *openingTimes;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *website;
@property (nonatomic, copy) NSString *facebook;
@property (nonatomic, copy) NSString *tripAdvisor;
@property (nonatomic, copy) NSString *fourSquare;
@property (nonatomic, copy) NSString *twitter;
@property (nonatomic, copy) NSString *cityId;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;
@property double voteAverage;
@property (nonatomic, strong) NSMutableArray *shopVotes;
@property (nonatomic, strong) NSMutableArray *shopPhotos;
@property (nonatomic, strong) NSMutableArray *shopDiscounts;
@property (nonatomic, strong) CP_ShopDiscount *baseDiscount;
@property (nonatomic, copy) NSString *cityName;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *username;
@property int totalViews;
@property (nonatomic, copy) NSString *lastViewDate;
@property int totalPurchases;
@property (nonatomic, copy) NSString *lastPurchaseDate;
@property int totalVisitors;
@property (nonatomic, strong) NSMutableArray *shopProducts;
@property BOOL featured;
@property double score;
@property (nonatomic, copy) NSString *dateInserted;
@property (nonatomic, copy) NSString *nextPaymentDate;
@property BOOL favourite;
@property int priceRange;
@property BOOL refundsParking;
@property (nonatomic, strong) NSMutableArray *staffPhotos;
@property (nonatomic, strong) NSMutableArray *voteGroups;
@property (nonatomic, copy) NSString *commoditiesSectorName;
@property BOOL newEntry;
@property int voteCount;
@property (nonatomic, copy) NSString *shortUrl;
@property (nonatomic, strong) CP_Texts *subtitles;
@property (nonatomic, strong) CP_Texts *shopDescriptions;
@property (nonatomic, strong) CP_Image *mainPhoto;
@property BOOL hasShopDiscounts;
@property CP_ShopStatus status;
@property (nonatomic, copy) NSString *cap;
@property CP_CommoditiesSectorType commoditiesSectorType;
@property BOOL hasBoxOfficeTickets;
@property (nonatomic, copy) NSString *lastModifiedDate;
@property (nonatomic, copy) NSString *expiryForNewEntry;

-(NSString*)toString:(BOOL)addNameWrap;
-(id)initWithArray:(NSArray*)array;
-(void)encodeWithCoder:(NSCoder *)encoder;
-(id)copyWithZone:(NSZone *)zone;
-(id)initWithCoder:(NSCoder *)decoder;
@end
