//  Created by Karen Lusinyan on 27/06/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

#import "PHNZCell.h"

@interface PHNZCell2 : PHNZCell

@property (readonly, weak, nonatomic) IBOutlet UIImageView *leftAccessoryView;
@property (readonly, weak, nonatomic) IBOutlet UIImageView *rightAccessoryView;
@property (readonly, weak, nonatomic) IBOutlet UILabel *label;
@property (readonly, weak, nonatomic) IBOutlet UILabel *value;
@property (readonly, weak, nonatomic) IBOutlet UIButton *leftAccessoryButton;
@property (readonly, weak, nonatomic) IBOutlet UIButton *rightAccessoryButton;

@end
