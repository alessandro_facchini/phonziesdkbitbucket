//  Created by Karen Lusinyan on 21/06/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

#import "PHNZContainerDelegate.h"
#import "PHNZOfflineController.h"

@interface PHNZContentController : UIViewController <PHNZContainerDelegate>

@property (nonatomic, assign) id<PHNZContainerDelegate> delegate;

@property (nonatomic, getter=isLoading) BOOL loading;
@property (nonatomic, getter=isDataLoaded) BOOL setDataLoaded;
@property (nonatomic, getter=isUpdateSilent) BOOL setUpdateSilent;
@property (nonatomic, getter=isReloadForced) BOOL setReloadForced;
@property (nonatomic, getter=isViewVisibile) BOOL setViewVisibile;
@property (nonatomic, strong) PHNZOfflineController *offlineController;

@end
