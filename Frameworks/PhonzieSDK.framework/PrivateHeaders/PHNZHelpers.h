//  Created by Karen Lusinyan on 05/06/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

#import <Foundation/Foundation.h>

#import "UIViewController+Ext.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

#define kAutomaticCellHeightCodeEnabled 1

@interface PHNZHelpers : NSObject

// setup new instance
+ (void)setup;

// reset previous instance
+ (void)reset;

///-----------------------
/// @name Database helpers
///-----------------------

+ (void)setupDatabase;

///---------------------
/// @name Layout helpers
///---------------------

+ (UIFont *)fontRegular:(CGFloat)size;

+ (UIFont *)fontBold:(CGFloat)size;

+ (UIColor *)themeColor;

+ (UIColor *)darkThemeColor;

+ (UIColor *)redThemeColor;

+ (UIColor *)orangeThemeColor;

+ (UIColor *)placeholderColor;

+ (UIColor *)disabledColor;

+ (NSString *)loremIpsum;

+ (NSString *)loremIpsumMedium;

+ (NSString *)loremIpsumShort;

+ (NSString *)rndLoremIpsum;

+ (NSString *)rndLoremIpsumShort;

+ (CGFloat)labelFontSize;

+ (CGFloat)valueFontSize;

///------------------------
/// @name Directory Helpers
///------------------------

+ (NSString *)moduleDirectory;

+ (NSBundle *)moduleBundle;

+ (NSString *)moduleBundleName;

+ (NSString *)moduleCrashDirectory;

+ (NSString *)moduleCrashLogFilePath;

///---------------------
/// @name Device helpers
///---------------------

+ (CGFloat)deviceWidthInPixels;

+ (CGFloat)deviceHeightInPixels;

+ (UIImage *)imageNamed:(NSString *)imageName;

///--------------------
/// @name Alert helpers
///--------------------

+ (id)showAlertWithTitle:(NSString *)title
                 message:(NSString *)message
                  sender:(id)sender
             cancelTitle:(NSString *)cancelTitle
            cancelAction:(void(^)(void))cancelAction;

+ (id)showAlertWithTitle:(NSString *)title
                 message:(NSString *)message
                  sender:(id)sender
             cancelTitle:(NSString *)cancelTitle
            confirmTitle:(NSString *)confirmTitle
            cancelAction:(void(^)(void))cancelAction
           confirmAction:(void(^)(void))confirmAction;

+ (id)showAlertWithTitle:(NSString *)title
                 message:(NSString *)message
                  sender:(id)sender
             cancelTitle:(NSString *)cancelTitle
            confirmTitle:(NSString *)confirmTitle
        otherButtonTitle:(NSString *)otherButtonTitle
            cancelAction:(void(^)(void))cancelAction
           confirmAction:(void(^)(void))confirmAction
       otherButtonAction:(void(^)(void))otherButtonAction;

+ (id)showInputAlertWithTitle:(NSString *)title
                      message:(NSString *)message
               alertViewStyle:(UIAlertViewStyle)alertViewStyle
               numberOfInputs:(NSInteger)numberOfInputs
                 placeholders:(NSArray *)placeholders
                keyboardTypes:(NSArray *)keyboardTypes
                       sender:(id)sender
                  cancelTitle:(NSString *)cancelTitle
                 confirmTitle:(NSString *)confirmTitle
                 cancelAction:(void(^)(void))cancelAction
                confirmAction:(void(^)(void))confirmAction
                  inputAction:(void(^)(NSArray *inputs))inputAction;

///-----------------------
/// @name Language helpers
///-----------------------

+ (int)currentLanguage;

+ (NSString *)localizedString:(NSString *)code;

+ (NSString *)missingCode:(NSString *)code;

///-------------------
/// @name Date helpers
///-------------------

+ (NSCalendar *)currentCalendar;

+ (NSDateComponents *)dateComponents:(NSDate *)date;

+ (NSDate *)dateIgnoringSeconds:(NSDate *)date;

+ (NSTimeInterval)currentTime;

+ (NSDateFormatter *)customDateFormatter:(NSString *)format;

+ (NSDateFormatter *)serverDateFormatter;

+ (NSDateFormatter *)clientDateFormatter;

+ (NSDateFormatter *)clientDateTimeFormatter;

+ (NSDateFormatter *)clientUIDateTimeFormatter; // HH:mm dd-MM-yyyy

+ (NSDateFormatter *)dateFormatter;

+ (NSDateFormatter *)timeFormatter;

+ (NSDate *)date:(NSString *)string;

+ (NSString *)stringDate:(NSDate *)date;

+ (NSString *)dateTimeString:(NSDate *)date;

+ (NSString *)fomrattedDate:(NSString *)string withFormat:(NSString *)format;

+ (NSString *)localizedFormattedDate:(NSString *)string;

+ (NSString *)localizedFormattedDateTime:(NSString *)string;

+ (NSString *)stringFromTimeInterval:(NSTimeInterval)interval;

+ (NSString *)formattedTime:(NSTimeInterval)time;

+ (NSString *)formattedCountDownTimer:(NSTimeInterval)time;

+ (BOOL)dateExperied:(NSDate *)date;

///---------------------
/// @name Number helpers
///---------------------

+ (NSNumberFormatter *)defaultCurrencyFormatter;

+ (NSString *)formattedAmountFromDouble:(double)amount;

+ (NSString *)formattedAmountFromString:(NSString *)string;

+ (double)doubleFromString:(NSString *)string;

+ (double)randomFloatFrom:(double)from to:(double)to;

+ (double)roundedNumber:(double)number up:(BOOL)up;

///-----------------------
/// @name Network helpers
///-----------------------

typedef void(^PHNZReachabilityUpdates)(BOOL offline);

+ (void)subscribeToReachabilityUpdatesForClass:(Class)aClass withBlock:(PHNZReachabilityUpdates)block;

+ (void)unsubscribeReachabilityUpdatesForClass:(Class)aClass;

+ (BOOL)isOffline;

+ (void)showNetworkError:(void(^)(void))completion;

///-----------------------
/// @name Location helpers
///-----------------------

typedef void(^PHNZLocationUpdates)(CLLocation *location);

+ (void)setDebugMode:(BOOL)debugMode;

+ (BOOL)isDebugMode;

+ (BOOL)isLocationUpdatesAvailable;

+ (void)showLocationServiceErrorMessage:(void(^)(void))completion;

+ (CLLocation *)getLocationFrom:(CLLocationCoordinate2D)coordinate;

+ (CLLocation *)getLocationFromLatitude:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude;

+ (CLLocation *)getLocationFromLatitudeString:(NSString *)latitudeString longitudeString:(NSString *)longitudeString;

+ (NSString *)stringFromDistance:(CLLocationDistance)distance;

+ (CLLocationDistance)distance:(CLLocation *)location;

+ (BOOL)distanceDidChangeSignificantly:(CLLocation *)newLocation oldLocation:(CLLocation *)oldLocation;

// call this method to obtain the current location
+ (CLLocation *)currentLocation;

+ (NSString *)currentLocationLatitudeString;

+ (NSString *)currentLocationLongitudeString;

+ (NSString *)latitudeString:(double)latitude;

+ (NSString *)longitudeString:(double)longitude;

// call this method to obtain the current location
// Note:: if location is not available it will be notified later by "block" callback
+ (CLLocation *)currentLocationForClass:(Class)aClass withBlock:(PHNZLocationUpdates)block;

+ (void)subscribeToLocationUpdatesForClass:(Class)aClass withBlock:(PHNZLocationUpdates)block;

+ (void)unsubscribeLocationUpdatesForClass:(Class)aClass;

///-------------------------
/// @name Controller helpers
///-------------------------

+ (void)presentController:(UIViewController *)viewController
                    title:(NSString *)title
                   sender:(id)sender
               completion:(void (^)(void))completion
             dismissBlock:(DismissBlock)dismissBlock;

+ (void)dismissController:(id)sender;

+ (void)dismissController:(id)sender withError:(NSError *)error;

///-------------------
/// @name View helpers
///-------------------

+ (UIView *)loadNibForClass:(Class)aClass;

+ (UIView *)loadNibForClass:(Class)aClass atIndex:(NSInteger)index;

///-----------------------
/// @name Tracking helpers
///-----------------------

+ (void)startCurrentSession;

+ (void)stopCurrentSession;

@end

///---------------------
/// @name Object helpers
///---------------------

@interface NSObject (PHNZUtils)

/**
 Returns an NSDictionary containing the properties of an object that are not nil.
 */
- (NSDictionary *)dictionaryRepresentation;

@end

///-----------------------
/// @name NSString helpers
///-----------------------

@interface NSString (PHNZUtils)

- (NSString *)trimmedString;

@end

///----------------------------------
/// @name NSMutableDicationry helpers
///----------------------------------

@interface NSMutableDictionary (PHNZUtils)

- (void)setObjectNotNull:(id)anObject forKey:(id<NSCopying>)aKey;

@end

///-------------------------------
/// @name UIViewController helpers
///-------------------------------

@interface UIViewController (PHNZUtils)

@property (nonatomic, strong) UIViewController *progress;

- (void)showHUD:(UIView *)container;

- (void)hideHUD:(UIView *)container;

@end

///--------------------------------
/// @name UIAlertController helpers
///--------------------------------

@interface UIAlertController (PHNZUtils)

- (void)showWithCompletion:(void(^)(void))completion;

@end

///--------------------------
/// @name UITableView helpers
///--------------------------

@interface UITableView (PHNZUtils)

- (void)registerCellForClass:(Class)aClass;

- (id)cellWithClass:(Class)aClass;

- (void)addGestureRecognizerWithTarget:(id)target selector:(SEL)selector;

@end

///-----------------------
/// @name UIButton helpers
///-----------------------

@interface UIButton (PHNZUtils)

- (void)setupLayoutWithTitle:(NSString *)title
                cornerRadius:(CGFloat)cornerRadius
             backgroundColor:(UIColor *)backgroundColor;

- (void)setupLayoutWithTitle:(NSString *)title
                cornerRadius:(CGFloat)cornerRadius
             backgroundColor:(UIColor *)backgroundColor
               disabledColor:(UIColor *)disabledColor;

@end

///----------------------
/// @name UILable helpers
///----------------------

@interface UILabel (PHNZUtils)

- (CGFloat)getHeight;

@end


