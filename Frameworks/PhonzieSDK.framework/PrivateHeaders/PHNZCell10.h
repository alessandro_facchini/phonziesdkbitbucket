//  Created by Karen Lusinyan on 03/07/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

#import "PHNZCell.h"

@interface PHNZCell10 : PHNZCell

@property (readonly, weak, nonatomic) IBOutlet UILabel *label1;
@property (readonly, weak, nonatomic) IBOutlet UILabel *value1;
@property (readonly, weak, nonatomic) IBOutlet UILabel *label2;
@property (readonly, weak, nonatomic) IBOutlet UILabel *value2;
@property (readonly, weak, nonatomic) IBOutlet UILabel *label3;
@property (readonly, weak, nonatomic) IBOutlet UILabel *value3;
@property (readonly, weak, nonatomic) IBOutlet UILabel *label4;
@property (readonly, weak, nonatomic) IBOutlet UILabel *value4;
@property (readonly, weak, nonatomic) IBOutlet UILabel *label5;
@property (readonly, weak, nonatomic) IBOutlet UILabel *value5;
@property (readonly, weak, nonatomic) IBOutlet UILabel *value6;
@property (readonly, weak, nonatomic) IBOutlet UILabel *value7;

- (void)setActive:(BOOL)active;

@end
