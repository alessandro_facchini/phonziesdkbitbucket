//  Created by Karen Lusinyan on 30/06/16.
//  Copyright © 2016 Lenis. All rights reserved.

#ifndef PHNZEnums_h
#define PHNZEnums_h

///------------------------
/// @name Network enums
///------------------------

typedef NS_ENUM(NSInteger, PHNZReloadMode) {
    PHNZReloadModeUnkown=0,
    PHNZReloadModeServerRequest,
    PHNZReloadModeLocal
};

///-------------------------
/// @name Layout enums
///-------------------------

typedef NS_ENUM(NSInteger, PHNZScrollDirection) {
    PHNZScrollDirectionUnknown=0,
    PHNZScrollDirectionUp,
    PHNZScrollDirectionDown
};

typedef NS_ENUM(NSInteger, PHNZPresentationMode) {
    PHNZPresentationModeDefault=0,
    PHNZPresentationModeModal,
    PHNZPresentationModePushed,
    PHNZPresentationModeOverCurrentContext,
    PHNZPresentationModeFromMenu,
    PHNZPresentationModeFromModule
};

/*
typedef NS_ENUM(NSInteger, PHNZContentPresentationMode) {
    PHNZContentPresentationModeDefault=0,
    PHNZContentPresentationModeModal,
    PHNZContentPresentationModePush,
    PHNZContentPresentationModeOverCurrentContext
};
//*/

///------------------------
/// @name Ticket enums
///------------------------

typedef NS_ENUM(NSInteger, PHNZTicketsPurchaseMode) {
    PHNZTicketsPurchaseModeBus=0,
    PHNZTicketsPurchaseModeBoxOffice
};

typedef NS_ENUM(NSInteger, PHNZBusTicketPurchasePresentationMode) {
    PHNZBusTicketPurchasePresentationModeTicketPurchase     = 1 << 0,
    PHNZBusTicketPurchasePresentationModeCarnetPurchase     = 1 << 1,
    PHNZBusTicketPurchasePresentationModeCarnetActivation   = 1 << 2
};

typedef NS_ENUM(NSInteger, PHNZTicketStatus) {
    PHNZTicketStatusActive=0,
    PHNZTicketStatusPending,
    PHNZTicketStatusExpired
};

#endif /* PHNZEnums_h */
