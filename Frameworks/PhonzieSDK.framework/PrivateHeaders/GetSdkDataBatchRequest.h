//------------------------------------------------------------------------------
// <wsdl2code-generated>
// This code was generated by http://www.wsdl2code.com iPhone version 2.1
// Date Of Creation: 6/9/2017 6:36:10 AM
//
//  Please dont change this code, regeneration will override your changes
///<wsdl2code-generated>
//
//------------------------------------------------------------------------------
//
//This source code was auto-generated by Wsdl2Code Version
//

#import <Foundation/Foundation.h>


@interface GetSdkDataBatchRequest : NSObject
{
}
@property (nonatomic, copy) NSString *clientVersion;
@property int clientVersionId;
@property int clientSystemId;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;
@property int clientProgramId;
@property BOOL getFullDetails;
@property (nonatomic, copy) NSString *lastModifiedDate;
@property int cityId;
@property int dictionaryVersion;
@property int language;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *password;

-(NSString*)toString:(BOOL)addNameWrap;
-(id)initWithArray:(NSArray*)array;
-(void)encodeWithCoder:(NSCoder *)encoder;
-(id)copyWithZone:(NSZone *)zone;
-(id)initWithCoder:(NSCoder *)decoder;
@end
