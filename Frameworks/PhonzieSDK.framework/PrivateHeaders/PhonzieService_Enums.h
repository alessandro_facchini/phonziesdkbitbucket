#import <Foundation/Foundation.h>


#ifndef _SoapProtocolVersion_
#define _SoapProtocolVersion_
typedef enum {
    	kSoapProtocolVersionDefault = 0,
    	kSoapProtocolVersionSoap11 = 1,
    	kSoapProtocolVersionSoap12 = 2,
} SoapProtocolVersion;
#endif

#ifndef _CP_CashbackPrizeUnit_
#define _CP_CashbackPrizeUnit_
typedef enum {
    	kCP_CashbackPrizeUnitPurchasesAbove10 = 0,
    	kCP_CashbackPrizeUnitRanking = 1,
    	kCP_CashbackPrizeUnitPurchases = 2,
    	kCP_CashbackPrizeUnitPurchasesInDifferentShops = 3,
    	kCP_CashbackPrizeUnitShopVotes = 4,
} CP_CashbackPrizeUnit;
#endif

#ifndef _CP_CommoditiesSectorType_
#define _CP_CommoditiesSectorType_
typedef enum {
    	kCP_CommoditiesSectorTypeShop = 0,
    	kCP_CommoditiesSectorTypeMuseum = 1,
    	kCP_CommoditiesSectorTypeExhibition = 2,
    	kCP_CommoditiesSectorTypeTheatre = 3,
} CP_CommoditiesSectorType;
#endif

#ifndef _CP_ShopStatus_
#define _CP_ShopStatus_
typedef enum {
    	kCP_ShopStatusInitiated = 0,
    	kCP_ShopStatusFinished = 1,
    	kCP_ShopStatusPublished = 2,
    	kCP_ShopStatusSuspended = 3,
    	kCP_ShopStatusDeleted = 4,
} CP_ShopStatus;
#endif

#ifndef _TransactionStatus_
#define _TransactionStatus_
typedef enum {
    	kTransactionStatusFailed = 0,
    	kTransactionStatusSucceeded = 1,
} TransactionStatus;
#endif

#ifndef _CP_TrafficTrackingCategory_
#define _CP_TrafficTrackingCategory_
typedef enum {
    	kCP_TrafficTrackingCategoryShop = 0,
    	kCP_TrafficTrackingCategoryProduct = 1,
    	kCP_TrafficTrackingCategoryUserPurchases = 2,
    	kCP_TrafficTrackingCategoryReview = 3,
    	kCP_TrafficTrackingCategoryStaffPhoto = 4,
    	kCP_TrafficTrackingCategoryShopPhoto = 5,
    	kCP_TrafficTrackingCategoryProductPhoto = 6,
    	kCP_TrafficTrackingCategoryParkingRefund = 7,
    	kCP_TrafficTrackingCategoryMainPhoto = 8,
    	kCP_TrafficTrackingCategoryProducts = 9,
    	kCP_TrafficTrackingCategoryShops = 10,
    	kCP_TrafficTrackingCategoryDiscounts = 11,
    	kCP_TrafficTrackingCategoryReviews = 12,
    	kCP_TrafficTrackingCategoryUserSession = 13,
    	kCP_TrafficTrackingCategoryUserLogin = 14,
    	kCP_TrafficTrackingCategoryWebpage = 15,
    	kCP_TrafficTrackingCategoryShopDescription = 16,
    	kCP_TrafficTrackingCategoryDiscount = 17,
    	kCP_TrafficTrackingCategoryShopPurchases = 18,
    	kCP_TrafficTrackingCategoryOperatingSystem = 19,
    	kCP_TrafficTrackingCategoryUserReviews = 20,
    	kCP_TrafficTrackingCategoryShopReviews = 21,
    	kCP_TrafficTrackingCategoryWebsite = 22,
    	kCP_TrafficTrackingCategoryFacebook = 23,
    	kCP_TrafficTrackingCategoryTwitter = 24,
    	kCP_TrafficTrackingCategoryFoursquare = 25,
    	kCP_TrafficTrackingCategoryTripadvisor = 26,
    	kCP_TrafficTrackingCategoryInstagram = 27,
    	kCP_TrafficTrackingCategoryGooglePlus = 28,
    	kCP_TrafficTrackingCategoryYoutube = 29,
    	kCP_TrafficTrackingCategoryEmail = 30,
    	kCP_TrafficTrackingCategoryFaq = 31,
    	kCP_TrafficTrackingCategoryLicenseTerms = 32,
    	kCP_TrafficTrackingCategorySupport = 33,
    	kCP_TrafficTrackingCategoryPrepaidParkingMain = 34,
    	kCP_TrafficTrackingCategoryPostpaidParkingMain = 35,
    	kCP_TrafficTrackingCategoryPhonzieTownMain = 36,
    	kCP_TrafficTrackingCategoryPublicTransportationMain = 37,
    	kCP_TrafficTrackingCategoryUserProfile = 38,
    	kCP_TrafficTrackingCategoryWallet = 39,
    	kCP_TrafficTrackingCategoryInAppPurchases = 40,
    	kCP_TrafficTrackingCategoryFilter = 41,
    	kCP_TrafficTrackingCategoryOrderBy = 42,
    	kCP_TrafficTrackingCategoryPhone = 43,
    	kCP_TrafficTrackingCategoryPrivacyPolicy = 44,
    	kCP_TrafficTrackingCategoryDirections = 45,
    	kCP_TrafficTrackingCategoryShopFilter = 46,
    	kCP_TrafficTrackingCategoryPurchaseQRCode = 47,
    	kCP_TrafficTrackingCategoryMap = 48,
    	kCP_TrafficTrackingCategorySlide = 49,
    	kCP_TrafficTrackingCategoryException = 50,
    	kCP_TrafficTrackingCategoryFindMyCar = 51,
    	kCP_TrafficTrackingCategoryFindMyCar_GetDirections = 52,
    	kCP_TrafficTrackingCategoryFindMyCar_CarPosition = 53,
    	kCP_TrafficTrackingCategoryFindMyCar_PrepaidParking = 54,
    	kCP_TrafficTrackingCategoryCarPosition = 55,
    	kCP_TrafficTrackingCategoryBoxOfficeMain = 56,
    	kCP_TrafficTrackingCategoryPrizeList = 57,
} CP_TrafficTrackingCategory;
#endif

#ifndef _CP_TrafficTrackingAction_
#define _CP_TrafficTrackingAction_
typedef enum {
    	kCP_TrafficTrackingActionView = 0,
    	kCP_TrafficTrackingActionClick = 1,
    	kCP_TrafficTrackingActionFavoriteOn = 2,
    	kCP_TrafficTrackingActionFavoriteOff = 3,
    	kCP_TrafficTrackingActionSwitchOn = 4,
    	kCP_TrafficTrackingActionSwitchOff = 5,
    	kCP_TrafficTrackingActionAdd = 6,
    	kCP_TrafficTrackingActionRemove = 7,
    	kCP_TrafficTrackingActionShare = 8,
    	kCP_TrafficTrackingActionSuccess = 9,
    	kCP_TrafficTrackingActionClose = 10,
    	kCP_TrafficTrackingActionOpen = 11,
    	kCP_TrafficTrackingActionUpdate = 12,
    	kCP_TrafficTrackingActionRegistration = 13,
    	kCP_TrafficTrackingActionRecharge = 14,
    	kCP_TrafficTrackingActionStart = 15,
    	kCP_TrafficTrackingActionStop = 16,
    	kCP_TrafficTrackingActionManage = 17,
    	kCP_TrafficTrackingActionLeavingPosition = 18,
    	kCP_TrafficTrackingActionMovement = 19,
} CP_TrafficTrackingAction;
#endif

#ifndef _CP_ShopViewType_
#define _CP_ShopViewType_
typedef enum {
    	kCP_ShopViewTypeView = 0,
    	kCP_ShopViewTypeViewDiscount = 1,
    	kCP_ShopViewTypeViewPhoto = 2,
    	kCP_ShopViewTypeViewVote = 3,
    	kCP_ShopViewTypeViewAround = 4,
    	kCP_ShopViewTypeAddDiscount = 5,
    	kCP_ShopViewTypeAddDiscountFavourite = 6,
    	kCP_ShopViewTypeAddFavourite = 7,
    	kCP_ShopViewTypeAddParkingRefund = 8,
    	kCP_ShopViewTypeAddPhoto = 9,
    	kCP_ShopViewTypeAddVote = 10,
    	kCP_ShopViewTypeAddShareOnSocial_Shop = 11,
    	kCP_ShopViewTypeAddAroundFavourite = 12,
    	kCP_ShopViewTypeAddShareOnSocial_Discount = 13,
    	kCP_ShopViewTypeAddMainPhoto = 14,
    	kCP_ShopViewTypeAddProduct = 15,
    	kCP_ShopViewTypeRemoveDiscount = 16,
    	kCP_ShopViewTypeRemoveDiscountFavourite = 17,
    	kCP_ShopViewTypeRemoveFavourite = 18,
    	kCP_ShopViewTypeRemoveParkingRefund = 19,
    	kCP_ShopViewTypeRemovePhoto = 20,
    	kCP_ShopViewTypeRemoveProduct = 21,
    	kCP_ShopViewTypeClickDiscount = 22,
    	kCP_ShopViewTypeClickProduct = 23,
    	kCP_ShopViewTypeClickShowCase = 24,
    	kCP_ShopViewTypeClickShop = 25,
    	kCP_ShopViewTypeClickAround = 26,
    	kCP_ShopViewTypePurchase = 27,
    	kCP_ShopViewTypeParkingRefund = 28,
    	kCP_ShopViewTypeReferrerShopOwnerToShopOwner = 29,
    	kCP_ShopViewTypeReferrerShopOwnerToUser = 30,
    	kCP_ShopViewTypeUserQueryAnswered = 31,
    	kCP_ShopViewTypeUserSessionClosed = 32,
    	kCP_ShopViewTypeWebsitePageOpened = 33,
    	kCP_ShopViewTypeUserLogin = 34,
    	kCP_ShopViewTypeUpdateShopDescription = 35,
    	kCP_ShopViewTypeUpdateDiscount = 36,
} CP_ShopViewType;
#endif

#ifndef _CP_AccountingRecordType_
#define _CP_AccountingRecordType_
typedef enum {
    	kCP_AccountingRecordTypeAccounts = 0,
    	kCP_AccountingRecordTypeParkings = 1,
    	kCP_AccountingRecordTypePurchases = 2,
    	kCP_AccountingRecordTypeRefunds = 3,
    	kCP_AccountingRecordTypeRecharges = 4,
    	kCP_AccountingRecordTypeBonusCredit = 5,
    	kCP_AccountingRecordTypeTickets = 6,
    	kCP_AccountingRecordTypeBoxOfficeTickets = 7,
} CP_AccountingRecordType;
#endif
@interface PhonzieService_Enums : NSObject
{
}
+(NSString*)SoapProtocolVersionToString:(SoapProtocolVersion)soapVersion;
+(SoapProtocolVersion)StringToSoapProtocolVersion:(NSString*)str;
+(NSString*)CP_CashbackPrizeUnitToString:(CP_CashbackPrizeUnit)unitType;
+(CP_CashbackPrizeUnit)StringToCP_CashbackPrizeUnit:(NSString*)str;
+(NSString*)CP_CommoditiesSectorTypeToString:(CP_CommoditiesSectorType)commoditiesSectorType;
+(CP_CommoditiesSectorType)StringToCP_CommoditiesSectorType:(NSString*)str;
+(NSString*)CP_ShopStatusToString:(CP_ShopStatus)status;
+(CP_ShopStatus)StringToCP_ShopStatus:(NSString*)str;
+(NSString*)TransactionStatusToString:(TransactionStatus)transactionStatus;
+(TransactionStatus)StringToTransactionStatus:(NSString*)str;
+(NSString*)CP_TrafficTrackingCategoryToString:(CP_TrafficTrackingCategory)trackingCategory;
+(CP_TrafficTrackingCategory)StringToCP_TrafficTrackingCategory:(NSString*)str;
+(NSString*)CP_TrafficTrackingActionToString:(CP_TrafficTrackingAction)trackingAction;
+(CP_TrafficTrackingAction)StringToCP_TrafficTrackingAction:(NSString*)str;
+(NSString*)CP_ShopViewTypeToString:(CP_ShopViewType)viewType;
+(CP_ShopViewType)StringToCP_ShopViewType:(NSString*)str;
+(NSString*)CP_AccountingRecordTypeToString:(CP_AccountingRecordType)recordsType;
+(CP_AccountingRecordType)StringToCP_AccountingRecordType:(NSString*)str;
@end
