//  Created by Simone Ferrini on 16/01/15.
//  Copyright (c) 2015 Lenis. All rights reserved.

@interface PHNZAPIEndpoint : NSObject

+ (NSString *)apiEndpoint;

@end
