//  Created by Karen Lusinyan on 26/06/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

typedef void(^ValueDidChange)(id value);

@interface PHNZCellResponder : NSObject

@property (readonly, nonatomic, strong) id responder;
@property (readonly, nonatomic, copy) ValueDidChange onValueDidChange;

@end

@class PHNZCell;
@interface PHNZCellConfig : NSObject

@property (nonatomic, strong) id cell;
@property (nonatomic) CGFloat rowHeight;
@property (nonatomic) CGFloat estimatedHeight;
@property (nonatomic) CGFloat minimumHeight;
@property (nonatomic, strong) PHNZCellResponder *responder;
@property (nonatomic) NSInteger cellType;
@property (nonatomic, getter=isEditing) BOOL editing;
@property (nonatomic) NSIndexPath *indexPath;
@property (nonatomic) PHNZCellConfig *inlineCellConfig;

@end

@interface PHNZCell : UITableViewCell

@property (readonly, nonatomic, strong) NSMutableArray *values;

- (void)reset;

- (PHNZCellResponder *)registerResponder:(id)responder onValueDidChange:(ValueDidChange)onValueDidChange;

- (void)responderDidEndDisplay:(id)sender;

- (void)responder:(id)responder didChangeValue:(id)value;

@end

