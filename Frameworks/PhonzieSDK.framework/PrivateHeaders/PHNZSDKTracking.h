//  Created by Karen Lusinyan on 26/06/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

#import "TrafficTrackingRequest.h"

typedef NS_ENUM(NSInteger, PHNZTrackingRequestState) {
    PHNZTrackingRequestStateUploaded=0,
    PHNZTrackingRequestStatePending
};

@interface TrafficTrackingRequestExt : TrafficTrackingRequest

@property (readonly, nonatomic, strong) NSNumber *identifier;
@property (readonly, nonatomic, strong) NSString *categoryName;
@property (readonly, nonatomic, strong) NSString *actionName;

@end

@interface PHNZSDKTracking : NSObject

+ (void)addTrackingEvent:(CP_TrafficTrackingCategory)category
                  action:(CP_TrafficTrackingAction)action
                   label:(NSString *)label
                   value:(NSString *)value;

+ (void)addTrackingEvent:(CP_TrafficTrackingCategory)category
                  action:(CP_TrafficTrackingAction)action
                   label:(NSString *)label
                   value:(NSString *)value
                  shopId:(int)shopId;

+ (NSArray *)trackingEventsBuffer;

+ (void)cleanUploadedTrackingRequests:(NSArray *)uploadedRequests;

@end
