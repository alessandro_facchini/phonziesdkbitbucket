// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Loc_LanguageDictionaryEntry.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@interface Loc_LanguageDictionaryEntryID : NSManagedObjectID {}
@end

@interface _Loc_LanguageDictionaryEntry : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) Loc_LanguageDictionaryEntryID *objectID;

@property (nonatomic, strong, nullable) NSString* key;

@property (nonatomic, strong, nullable) NSString* label;

@property (nonatomic, strong, nullable) NSNumber* language;

@property (atomic) int64_t languageValue;
- (int64_t)languageValue;
- (void)setLanguageValue:(int64_t)value_;

@end

@interface _Loc_LanguageDictionaryEntry (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveKey;
- (void)setPrimitiveKey:(NSString*)value;

- (NSString*)primitiveLabel;
- (void)setPrimitiveLabel:(NSString*)value;

- (NSNumber*)primitiveLanguage;
- (void)setPrimitiveLanguage:(NSNumber*)value;

- (int64_t)primitiveLanguageValue;
- (void)setPrimitiveLanguageValue:(int64_t)value_;

@end

@interface Loc_LanguageDictionaryEntryAttributes: NSObject 
+ (NSString *)key;
+ (NSString *)label;
+ (NSString *)language;
@end

NS_ASSUME_NONNULL_END
