//
//  Loc_DictionaryResponse+CoreDataClass.h
//  
//
//  Created by Alterbit  on 12/11/2020.
//
//  This file was automatically generated and should not be edited.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Loc_DictionaryResponse : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Loc_DictionaryResponse+CoreDataProperties.h"
