//
//  Loc_DictionaryResponse+CoreDataProperties.m
//  
//
//  Created by Alterbit  on 12/11/2020.
//
//  This file was automatically generated and should not be edited.
//

#import "Loc_DictionaryResponse+CoreDataProperties.h"

@implementation Loc_DictionaryResponse (CoreDataProperties)

+ (NSFetchRequest<Loc_DictionaryResponse *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Loc_DictionaryResponse"];
}

@dynamic language;
@dynamic last_update;
@dynamic version;

@end
