//
//  Loc_DictionaryResponse+CoreDataProperties.h
//  
//
//  Created by Alterbit  on 12/11/2020.
//
//  This file was automatically generated and should not be edited.
//

#import "Loc_DictionaryResponse+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Loc_DictionaryResponse (CoreDataProperties)

+ (NSFetchRequest<Loc_DictionaryResponse *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *language;
@property (nullable, nonatomic, copy) NSDate *last_update;
@property (nullable, nonatomic, copy) NSNumber *version;

@end

NS_ASSUME_NONNULL_END
