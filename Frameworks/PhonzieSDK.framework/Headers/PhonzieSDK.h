//  Created by Karen Lusinyan on 16/06/2017.
//  Copyright © 2017 Karen Lusinyan. All rights reserved.

#import <UIKit/UIKit.h>

//! Project version number for PhonzieSDK.
FOUNDATION_EXPORT double PhonzieSDKVersionNumber;

//! Project version string for PhonzieSDK.
FOUNDATION_EXPORT const unsigned char PhonzieSDKVersionString[];

@class PhonziePaymentMethod;

/*!
 * @brief PhonzieUser interface is used to define the Phonzie user
 */
@interface PhonzieUser : NSObject <NSCoding>

@property (readonly, nonatomic, copy) NSString *username;
@property (readonly, nonatomic, copy) NSString *name;
@property (readonly, nonatomic, copy) NSString *surname;
@property (readonly, nonatomic, copy) NSString *profilePhotoUrl;
@property (readonly, nonatomic) double credit;
@property (readonly, nonatomic) double autoTopUp;
@property (readonly, nonatomic) double autoTopUpThreshold;
@property (readonly, nonatomic, strong) PhonziePaymentMethod *currentPaymentMethod;
@property (readonly, nonatomic, strong) NSArray *carLicenses;
@property (readonly, nonatomic) BOOL isAutoTopUpOn;

@end

/*!
 * @brief PhonziePaymentMethod interface is used to recharge the user's wallet
 */
@interface PhonziePaymentMethod : NSObject <NSCoding>

@property (readonly, nonatomic, copy) NSString *methodId;
@property (readonly, nonatomic, copy) NSString *descrForUser;
@property (readonly, nonatomic, copy) NSString *descrServerCode;
@property (readonly, nonatomic) BOOL enrollNeeded;
@property (readonly, nonatomic) BOOL enrollmentDone;
@property (readonly, nonatomic) double additionalCost;
@property (readonly, nonatomic) double additionalCostLimit;
@property (readonly, nonatomic, copy) NSString *enrollmentInfo;
@property (readonly, nonatomic, copy) NSString *warningForAdditionalCost;

@end

/*!
 * @brief PhonzieCity interface is used to define the city
 */
@interface PhonzieCity : NSObject <NSCoding>

@property (readonly, nonatomic, copy) NSString *cityId;
@property (readonly, nonatomic, copy) NSString *cityName;
@property (readonly, nonatomic, copy) NSString *province;
@property (readonly, nonatomic) BOOL cardExpositionMode;
@property (readonly, nonatomic) BOOL searchByAreaRequired;
@property (readonly, nonatomic) BOOL requireSecondConfirmOnPurchase;
@property (readonly, nonatomic) double latitude;
@property (readonly, nonatomic) double longitude;
@property (readonly, nonatomic) BOOL requiresSpotNumber;

@end

/*!
 * @brief PhonzieParkingArea interface used to define the parking area
 */
@interface PhonzieParkingArea : NSObject <NSCoding>

@property (readonly, nonatomic, copy) NSString *areaId;
@property (readonly, nonatomic, copy) NSString *cityId;
@property (readonly, nonatomic, copy) NSString *areaCode;
@property (readonly, nonatomic) double latitude;
@property (readonly, nonatomic) double longitude;
@property (readonly, nonatomic) NSInteger maximumDurationMinutes;
@property (readonly, nonatomic, copy) NSString *fareDescr;
@property (readonly, nonatomic, copy) NSString *areaDescr;
@property (readonly, nonatomic, copy) NSString *confirmationMessage;
@property (readonly, nonatomic, copy) NSString *confirmationMessageLink;

@end

/*!
 * @brief PhonzieParkingInfo interface is used to define the partking informations
 */
@interface PhonzieParkingInfo : NSObject <NSCoding>

@property (readonly, nonatomic, copy) NSString *parkingId;
@property (readonly, nonatomic, copy) NSString *areaId;
@property (readonly, nonatomic, copy) NSString *cityId;
@property (readonly, nonatomic, copy) NSString *carLicense;
@property (readonly, nonatomic, copy) NSDate *startDate;
@property (readonly, nonatomic, copy) NSDate *endDate;
@property (readonly, nonatomic) double amount;
@property (readonly, nonatomic) double latitude;
@property (readonly, nonatomic) double longitude;
@property (readonly, nonatomic, copy) NSString *cityName;
@property (readonly, nonatomic, copy) NSString *spotNumber;
@property (readonly, nonatomic, copy) NSString *streetAddress;
@property (readonly, nonatomic, copy) NSString *areaCode;
@property (readonly, nonatomic) NSInteger minutesLeft;
@property (readonly, nonatomic) BOOL stoppedByUser;
@property (readonly, nonatomic) double creditRefunded;
@property (readonly, nonatomic) double bonusRefunded;
@property (readonly, nonatomic) BOOL isActive;

@end

/*!
 * @brief PhonzieTransactionStatus shows the status of transaction
 */
typedef NS_ENUM(NSInteger, PhonzieTransactionStatus) {
    PhonzieTransactionStatusFailure=0,
    PhonzieTransactionStatusSuccess=1
};

/*!
 * @brief PhonzieAPIResponse is a superclass for all response objects
 */
@interface PhonzieAPIResponse : NSObject <NSCoding>

@property (readonly, nonatomic, copy) NSString *failureMessage;
@property (readonly, nonatomic, copy) NSString *failureReason;
@property (readonly, nonatomic) PhonzieTransactionStatus status;

@end

/*!
 * @brief PhonzieGetUserResponse is a response for "getUser:" result
 */
@interface PhonzieGetUserResponse : PhonzieAPIResponse

@property (readonly, nonatomic, strong) PhonzieUser *user;

@end

/*!
 * @brief PhonzieGetCitiesResponse is a response for "getCities:longitude:completion:" result
 */
@interface PhonzieGetCitiesResponse : PhonzieAPIResponse

@property (readonly, nonatomic, strong) NSArray *cities;

@end

/*!
 * @brief PhonzieGetDataForParkingResponse is a response for "getDataForParking:longitude:cityId:getFullDetails:completion:" result
 */
@interface PhonzieGetDataForParkingResponse : PhonzieAPIResponse

@property (readonly, nonatomic, strong) PhonzieUser *user;
@property (readonly, nonatomic, strong) NSArray *paymentMethods;
@property (readonly, nonatomic, strong) NSArray *userParkings;
@property (readonly, nonatomic, strong) NSArray *cities;
@property (readonly, nonatomic, strong) PhonzieCity *selectedCity;
@property (readonly, nonatomic, strong) NSArray *areas;
@property (readonly, nonatomic, strong) PhonzieParkingArea *closestArea;
@property (readonly, nonatomic) BOOL isInsideCityBoundaries;

@end

/*!
 * @brief PhonzieGetPrepaidParkingAmountResponse is a response for "getPrepaidParkingAmount:cityId:cityId:areaId:spotNumber:durationMinutes:latitude:longitude:completion:" result
 */
@interface PhonzieGetPrepaidParkingAmountResponse : PhonzieAPIResponse

@property (readonly, nonatomic, strong) NSDate *expiryDate;
@property (readonly, nonatomic) double amount;
@property (readonly, nonatomic, strong) PhonzieParkingArea *parkingArea;

@end

/*!
 * @brief PhonzieStartPrepaidParkingResponse is a response for "startPrepaidParking:cityId:cityId:areaId:spotNumber:durationMinutes:latitude:longitude:completion:" result
 */
@interface PhonzieStartPrepaidParkingResponse : PhonzieAPIResponse

@property (readonly, nonatomic, strong) PhonzieParkingInfo *parkingInfo;

@end

/*!
 * @brief PhonzieStopPrepaidParkingResponse is a response for "stopPrepaidParking:completion:" result
 */
@interface PhonzieStopPrepaidParkingResponse : PhonzieAPIResponse

@property (readonly, nonatomic, strong) PhonzieParkingInfo *parkingInfo;

@end

/*!
 * @brief PhonzieExtendPrepaidParkingResponse is a response for "extendPrepaidParking:additionalMinutes:completion:" result
 */
@interface PhonzieExtendPrepaidParkingResponse : PhonzieAPIResponse

@property (readonly, nonatomic, strong) PhonzieParkingInfo *parkingInfo;

@end

/*!
 * @brief PhonzieGetUserParkingsResponse is a response for "getUserParkings:" result
 */
@interface PhonzieGetUserParkingsResponse : PhonzieAPIResponse

@property (readonly, nonatomic, strong) NSArray *userParkings;

@end

/*!
 * @brief PhonzieSetCurrentPaymentMethodResponse is a response for "setCurrentPaymentMethod:completion:" result
 */
@interface PhonzieSetCurrentPaymentMethodResponse : PhonzieAPIResponse

@property (readonly, nonatomic, strong) PhonzieUser *user;

@end

/*!
 * @brief PhonzieRechargeCreditResponse is a response for "rechargeCredit:completion:" result
 */
@interface PhonzieRechargeCreditResponse : PhonzieAPIResponse

@property (readonly, nonatomic) double currentCredit;
@property (readonly, nonatomic, copy) NSString *enrollmentUrl;

@end

/*!
 * @brief PhonzieRevokeCurrentEnrollmentResponse is a response for "revokeCurrentEnrollment:" result
 */
@interface PhonzieRevokeCurrentEnrollmentResponse : PhonzieAPIResponse

@property (readonly, nonatomic) BOOL success;

@end

/*!
 * @brief PhonzieSetAutoTopUpResponse is a response for "setAutoTopUp:completion:" result
 */
@interface PhonzieSetAutoTopUpResponse : PhonzieAPIResponse

@property (readonly, nonatomic, strong) PhonzieUser *user;

@end

/*!
 * @brief PhonzieSDKParkingInfo interface to define the callback object when parking started, stopped or extanded.
 */
@interface PhonzieSDKParkingInfo : NSObject

@property (readonly, nonatomic, copy) NSString *parkingId;
@property (readonly, nonatomic, copy) NSString *username;
@property (readonly, nonatomic, copy) NSString *carLicense;
@property (readonly, nonatomic, copy) NSString *cityId;
@property (readonly, nonatomic, copy) NSString *cityName;
@property (readonly, nonatomic, copy) NSString *areaId;
@property (readonly, nonatomic, copy) NSString *areaName;
@property (readonly, nonatomic, copy) NSString *spotNumber;
@property (readonly, nonatomic) NSInteger parkingDuration;
@property (readonly, nonatomic, strong) NSDate *startDate;
@property (readonly, nonatomic, strong) NSDate *endDate;
@property (readonly, nonatomic) double amount;
@property (readonly, nonatomic) double amountRefunded;

@end

typedef void(^OnUserDidStartParking)(PhonzieSDKParkingInfo *parkingInfo);
typedef void(^OnUserDidStopParking)(PhonzieSDKParkingInfo *parkingInfo);
typedef void(^OnUserDidExtendParking)(PhonzieSDKParkingInfo *parkingInfo);

@protocol PhonzieSDKDelegate;

// In this header, you should import all the public headers of your framework using statements like #import <PhonzieSDK/PublicHeader.h>

@interface PhonzieSDK : NSObject

/*!
 * @brief Phonzie Developer Account Key received at registration time `required`.
 */
@property (readonly, nonatomic, copy) NSString *developerKey;

/*!
 * @brief Your customer email address `required`.
 */
@property (nonatomic, copy) NSString *username;

/*!
 * @brief Pass device verificaton code if needed.
 */
@property (nonatomic, copy) NSString *deviceVerificationCode;

/*!
 * @brief Set true to switch to test environment, default is false.
 */
@property (nonatomic) BOOL isTest;

/*!
 * @brief Specify the car license, it will be set as a default value.
 */
@property (nonatomic, copy) NSString *carLicense;

/*!
 * @brief Specify the parking end date, default is nil.
 */
@property (nonatomic, strong) NSDate *parkingEndDate;

/*!
 * @brief Specifies the default parking duration in minutes, default is 60 min.
 */
@property (nonatomic) NSInteger minutesForParkingDuration;

/*!
 * @brief Specifies the parking extension time in minutes, default is 30 min.
 */
@property (nonatomic) NSInteger minutesForParkingExtension;

/*!
 * @brief Specifies the time when user will be notified of expiring parking, default is 10 min. If disablePushNotifications is set to true this parameter will be ignored.
 */
@property (nonatomic) NSInteger minutesForParkingExpiryNotification;

/*!
 * @brief Enable/disable parking zone auto detection, based on GPS coordinates, default is false.
 */
@property (nonatomic) BOOL disableAutoZoneDetection;

/*!
 * @brief Enable/disable push notifications, default is false.
 */
@property (nonatomic) BOOL disablePushNotificatons;

/*!
 * @brief Specify the current location latitude, as default the user’s current location is considered if location service is granted.
 */
@property (nonatomic) double latitude;

/*!
 * @brief Specify the current location longitude, as default the user’s current location is considered if location service is granted.
 */
@property (nonatomic) double longitude;

/*!
 * @brief Selects the specific city at start point. By passing this parameter, the GPS coordinates will be ignored, default is nil. Please, contact to support@phonzie.eu to obtain the cityId.
 */
@property (nonatomic, copy) NSString *cityId;

/*!
 * @brief PhonzieSDK provides its delegate to receive user' parking events such that: startParking, stopParking and extendParking.
 */
@property (nonatomic, unsafe_unretained) id<PhonzieSDKDelegate> delegate;

/*!
 * @brief Callback for user start parking event
 */
@property (nonatomic, copy) OnUserDidStartParking onUserDidStartParking;

/*!
 * @brief Callback for user stop parking event
 */
@property (nonatomic, copy) OnUserDidStopParking onUserDidStopParking;

/*!
 * @brief Callback for user extend parking event
 */
@property (nonatomic, copy) OnUserDidExtendParking onUserDidExtendParking;

/*!
 * @discussion Initialization of PhonzieSDK.
 * @param developerKey Phonzie Developer Account Key received at registration time `required`.
 * @return The instance of PhonzieSDK.
 */
+ (instancetype)setupSDK:(NSString *)developerKey;

/*!
 * @discussion Call this method to start parking.
 */
- (void)startParking;

/*!
 * @discussion Call this method to start parking.
 * @param sender is the controller which will present the sdk's main controller. If nil, then the sdk's main controller will presented by keyWindow.rootViewController
 * @param presented will return as soon as controller did show.
 * @param dismissed will return as soon as controller did hide.
 */
- (void)startParking:(id)sender
           presented:(void(^)(BOOL started))presented
           dismissed:(void(^)(BOOL finished, NSError *error))dismissed;

/*!
 * @discussion Call this method to check if certain area is covered by PhonzieSDK.
 * @param latitude GSP coordinate.
 * @param longitude GPS coordinate.
 * @param completion async callback will return the result if error is nil
 */
- (void)isCovered:(double)latitude
        longitude:(double)longitude
       completion:(void (^)(BOOL covered, NSError *error))completion;

/*!
 * @discussion Call this method to get current user
 * @param completion async callback will return the result if error is nil
 */
- (void)getUser:(void (^)(PhonzieGetUserResponse *response, NSError *error))completion;

/*!
 * @discussion Call this method to get the list of cities where the Phonzie is present
 * @param latitude GSP coordinate.
 * @param longitude GPS coordinate.
 * @param completion async callback will return the result if error is nil
 */
- (void)getCities:(double)latitude
        longitude:(double)longitude
       completion:(void (^)(PhonzieGetCitiesResponse *response, NSError *error))completion;

/*!
 * @discussion Call this method to get all or partial information to start parking
 * @param latitude GSP coordinate.
 * @param longitude GPS coordinate.
 * @param cityId current city id obtained from getCities:longitudecompletion:.
 * @param getFullDetails set true to get full detailed information to start parking
 * @param completion async callback will return the result if error is nil
 */
- (void)getDataForParking:(double)latitude
                longitude:(double)longitude
                   cityId:(NSString *)cityId
           getFullDetails:(BOOL)getFullDetails
               completion:(void (^)(PhonzieGetDataForParkingResponse *response, NSError *error))completion;

/*!
 * @discussion Call this method to get all or partial information to start parking
 * @param carLicense is the user's car car license
 * @param cityId current city id obtained from getCities:longitudecompletion:.
 * @param areaId current area id obtained from getDataForParking:cityId:getFullDetails:completion.
 * @param spotNumber parking spot number, default is nil.
 * @param latitude GSP coordinate.
 * @param longitude GPS coordinate.
 * @param completion async callback will return the result if error is nil
 */
- (void)getPrepaidParkingAmount:(NSString *)carLicense
                         cityId:(NSString *)cityId
                         areaId:(NSString *)areaId
                     spotNumber:(NSString *)spotNumber
                durationMinutes:(NSInteger)durationMinutes
                       latitude:(double)latitude
                      longitude:(double)longitude
                     completion:(void (^)(PhonzieGetPrepaidParkingAmountResponse *response, NSError *error))completion;

/*!
 * @discussion Call this method to start parking
 * @param carLicense is the user's car car license
 * @param cityId current city id obtained from getCities:longitudecompletion:.
 * @param areaId current area id obtained from getDataForParking:cityId:getFullDetails:completion.
 * @param spotNumber parking spot number, default is nil.
 * @param durationMinutes set current parking duration in minutes, defualt is 60 min
 * @param latitude GSP coordinate.
 * @param longitude GPS coordinate.
 * @param completion async callback will return the result if error is nil
 */
- (void)startPrepaidParking:(NSString *)carLicense
                     cityId:(NSString *)cityId
                     areaId:(NSString *)areaId
                 spotNumber:(NSString *)spotNumber
            durationMinutes:(NSInteger)durationMinutes
                   latitude:(double)latitude
                  longitude:(double)longitude
                 completion:(void (^)(PhonzieStartPrepaidParkingResponse *response, NSError *error))completion;

/*!
 * @discussion Call this method to stop parking
 * @param parkingId the current parking identifier
 * @param completion async callback will return the result if error is nil
 */
- (void)stopPrepaidParking:(NSString *)parkingId
                completion:(void (^)(PhonzieStopPrepaidParkingResponse *response, NSError *error))completion;

/*!
 * @discussion Call this method to extend parking
 * @param parkingId the current parking identifier
 * @param additionalMinutes set addional minutes to extend the parking, default is 30 min
 * @param completion async callback will return the result if error is nil
 */
- (void)extendPrepaidParking:(NSString *)parkingId
           additionalMinutes:(NSInteger)additionalMinutes
                  completion:(void (^)(PhonzieExtendPrepaidParkingResponse *response, NSError *error))completion;

/*!
 * @discussion Call this method to get the user's last parkings (last 10 parkings)
 * @param completion async callback will return the result if error is nil
 */
- (void)getUserParkings:(void (^)(PhonzieGetUserParkingsResponse *response, NSError *error))completion;

/*!
 * @discussion Call this method to set user's default payment method. To get full list of available payment methods first call getUser: or
 getDataForParking:longitude:cityId:getFullDetails:completion:
 * @param completion async callback will return the result if error is nil
 */
- (void)setCurrentPaymentMethod:(NSString *)paymentPreference
                     completion:(void (^)(PhonzieSetCurrentPaymentMethodResponse *response, NSError *error))completion;

/*!
 * @discussion Call this method to recharge the user's wallet
 * @param amount the new recharge amount
 * @param completion async callback will return the result if error is nil
 */
- (void)rechargeCredit:(double)amount
            completion:(void (^)(PhonzieRechargeCreditResponse *response, NSError *error))completion;

/*!
 * @discussion Call this method to revoke current payment method
 * @param completion async callback will return the result if error is nil
 */
- (void)revokeCurrentEnrollment:(void (^)(PhonzieRevokeCurrentEnrollmentResponse *response, NSError *error))completion;

/*!
 * @discussion Call this method to set auto top up on/off
 * @param isOn the flag to set
 * @param completion async callback will return the result if error is nil
 */
- (void)setAutoTopUp:(BOOL)isOn
          completion:(void (^)(PhonzieSetAutoTopUpResponse *response, NSError *error))completion;

/*!
 * @discussion Call this method to reset all cached parameteres passed to PhonzieSDK.
 */
- (void)reset;

@end

@protocol PhonzieSDKDelegate <NSObject>

@optional
/*!
 * @discussion Called when user did start parking
 * @param phonzieSDK invoker instance
 * @param userInfo user's parking information
 */
- (void)phonzieSDK:(PhonzieSDK *)phonzieSDK userDidStartParking:(PhonzieSDKParkingInfo *)userInfo;

/*!
 * @discussion Called when user did stop parking
 * @param phonzieSDK invoker instance
 * @param userInfo user's parking information
 */
- (void)phonzieSDK:(PhonzieSDK *)phonzieSDK userDidStopParking:(PhonzieSDKParkingInfo *)userInfo;

/*!
 * @discussion Called when user did extend parking
 * @param phonzieSDK invoker instance
 * @param userInfo user's parking information
 */
- (void)phonzieSDK:(PhonzieSDK *)phonzieSDK userDidExtendParking:(PhonzieSDKParkingInfo *)userInfo;

@end
