//
//  Loc_LanguageDictionaryEntry+CoreDataProperties.h
//  
//
//  Created by Alterbit  on 12/11/2020.
//
//  This file was automatically generated and should not be edited.
//

#import "Loc_LanguageDictionaryEntry+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Loc_LanguageDictionaryEntry (CoreDataProperties)

+ (NSFetchRequest<Loc_LanguageDictionaryEntry *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *key;
@property (nullable, nonatomic, copy) NSString *label;
@property (nullable, nonatomic, copy) NSNumber *language;

@end

NS_ASSUME_NONNULL_END
