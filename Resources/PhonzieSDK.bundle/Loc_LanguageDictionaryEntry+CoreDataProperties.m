//
//  Loc_LanguageDictionaryEntry+CoreDataProperties.m
//  
//
//  Created by Alterbit  on 12/11/2020.
//
//  This file was automatically generated and should not be edited.
//

#import "Loc_LanguageDictionaryEntry+CoreDataProperties.h"

@implementation Loc_LanguageDictionaryEntry (CoreDataProperties)

+ (NSFetchRequest<Loc_LanguageDictionaryEntry *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Loc_LanguageDictionaryEntry"];
}

@dynamic key;
@dynamic label;
@dynamic language;

@end
